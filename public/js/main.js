(function () {
	"use strict";

	var treeviewMenu = $('.app-menu');

	// Toggle Sidebar
	$('[data-toggle="sidebar"]').click(function(event) {
		event.preventDefault();
		$('.app').toggleClass('sidenav-toggled');

		// Add value to localStorage sidenav-toggled
		localStorage.setItem('sidenav-toggled', $('.app').hasClass('sidenav-toggled'));
	});

	if ($( window ).width() >= 767) {

		// Check localStorage sidenav-toggled
		if (localStorage.getItem('sidenav-toggled') === 'true') {
			$('.app').addClass('sidenav-toggled');
		} else {
			$('.app').removeClass('sidenav-toggled');
		}

	}

	// Activate sidebar treeview toggle
	$("[data-toggle='treeview']").click(function(event) {
		event.preventDefault();
		if(!$(this).parent().hasClass('is-expanded')) {
			treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
		}
		$(this).parent().toggleClass('is-expanded');
	});

	// Set initial active toggle
	$("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');

	//Activate bootstrip tooltips
	$("[data-toggle='tooltip']").tooltip();

})();

function pageReload() {
	location.reload();
};

$('#searchModal').on('shown.bs.modal', function () {
	$('#search_box').focus()
})

function deleteTableRow(id, event) {
	event.preventDefault();
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this data!",
		type: "warning",
		showCancelButton: true,
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No, cancel plx!",
		closeOnConfirm: false,
		closeOnCancel: false,
	}, function(isConfirm) {
		if (isConfirm) {
			// swal("Deleted!", "Your imaginary file has been deleted.", "success"),
			// setTimeout( function () { 
				$("#delete-table-row-"+id).submit();
			// }, 800);
		} else {
			swal("Cancelled", "Your data is safe :)", "error");
		}
	});
};

function isMalicious(value, el, encrypt, encrypt1)
{
	// var table = $('.table').DataTable({
	// 	"aaSorting": [],
	// });
	var url = BASE_URL+"/case-record/"+encrypt+"/is-malicious";
	var data = {
		value : value,
		check : ($('#'+value).is(':checked') ? 'true' : 'false'),
		encrypt1 : encrypt1,
	};

	$.ajax({
		url: url,
		method: "POST",
		data : data,
	}).done(function(responce) {
		// console.log(responce);
		// var table = tables.tables('#table'+responce.tableNo);
		// console.log(table);
		// var table = tables.tables('#DataTables_Table_'+responce.tableNo);
		var selectedRow = $('#'+value).closest('tr');
		if (responce.check == 'true') {
			$('#'+value).closest('tr').addClass(responce.name);
		} else {
			$('#'+value).closest('tr').removeClass(responce.name);
		}
		// table.cell(selectedRow, 1).data('Value Change').draw();
	});
};

// $(document).ready(function(){
// 	$('ul li a').click(function(){
// 		$('li a').removeClass("active");
// 		$(this).addClass("active");
// 	});
// });
// console.log(location.href);
// $(".app-menu>li").each(function() {
// 	var navItem = $(this);
// 	if (navItem.find("a").attr("href") == location.href) {
// 		navItem.children('a').addClass("active");
// 	}
// });