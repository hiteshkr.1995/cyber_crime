@extends('panel.layouts.app')

@section('title', 'Search Imei')


@push('plugins')
<style type="text/css">
	table tbody { display:block; max-height:450px; overflow-y:scroll; }
	table thead, table tbody tr { display:table; width:100%; table-layout:fixed; }
</style>
@endpush
@section('content')
<main class="app-content">
	<div class="app-title">
		<div class="app-title-item">
			<h1>Search Imei</h1>
		</div>
		<div class="app-title-item" style="width: 60%;">
			<form>
				<input class="form-control form-control-lg" name="value" required type="text" autofocus placeholder="Enter IMEI to search">
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			@isset($results)
				@php $table = NULL; @endphp
				@foreach($results as $cdrKey => $cdr)
					@if(count($cdr) > 0)
						@php $table += 1; @endphp
						<div class="tile">
							<div class="table-scrolle">
								<h3 class="tile-title">{{ $cdrKey }}</h3>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Id</th>
											<th>Case Record</th>
										</tr>
									</thead>
									<tbody>
										@foreach($cdr as $key => $value)
											<tr>
												<td>{{ $value->id }}</td>
												<td>{{ $value->case_record_id }}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					@endif
				@endforeach
				@if($table == NULL)
					<div class="tile">
						<center>
							<h1> ( {{ request()->value }} ) Not Found!</h1>
						</center>
					</div>
				@endif
			@else
				<div class="tile">
					<center>
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
							<path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z"/>
							<path d="M0 0h24v24H0z" fill="none"/>
						</svg>
						<h1>Enter IMEI to search</h1>
					</center>
				</div>
			@endisset
		</div>
	</div>
</main>
@endsection