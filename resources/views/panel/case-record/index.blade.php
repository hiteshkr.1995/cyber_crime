@extends('panel.layouts.app')

@section('title', 'CaseRecord')

@push('plugins')
<link href="{{ asset('datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endpush

@section('content')
<main class="app-content">
	<div class="app-title">
		<div>
			<h1>Case Record</h1>
			<p>Details of all the Cases</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					Data Table Example
					@if (Auth::user()->privilege())
						<a class="btn btn-info float-right" href="{{ route('case-record.create') }}">Add CaseRecord</a>
					@endif
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Case ID</th>
									<th>Name</th>
									<th>Mobile Number</th>
									<th>Case Status</th>
									<th>Case Type</th>
									<th>Assigned To</th>
									{{--<th>PDF</th>--}}
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@isset($caseRecord)
									@foreach ($caseRecord as $key => $value)
										<tr >
											<td><a href="{{ route('case-record.show', param_encrypt($value->id)) }}">{{ $value->case_id }}</a></td>
											<td>{{ $value->applicant_name ?? '' }}</td>
											<td>{{ $value->applicant_mobile ?? '' }}</td>
											<td>{{ ($value->case_status === 1)?"Pending":"Completed" }}</td>
											<td>{{ $value->caseType->name ?? '' }}</td>
											<td>{{ $value->assignedTo->name ?? 'Not assigned'}}</td>
											{{--<td>
												<a href="{{ route('case-record.pdf', param_encrypt($value->id)) }}" target="_blank">
													<button class="btn btn-sm btn-danger clearfix">
														<span class="fa fa-file-pdf-o"></span> Export to PDF
													</button>
												</a>
											</td>--}}
											<td width="15%">
												<div class="row justify-content-center">
													@if (Auth::user()->privilege())
														<form id="delete-table-row-{{ param_encrypt($value->id) }}" action="{{ route('case-record.destroy', param_encrypt($value->id)) }}" method="POST">
															@csrf
															@method('DELETE')
															<button onclick="deleteTableRow('{{ param_encrypt($value->id) }}', event)" class="btn-sm btn-outline-danger" type="button">Delete</button>
														</form>
													@endif
													<a href="{{ route('case-record.edit', param_encrypt($value->id)) }}">
														<button class="btn-sm btn-outline-info" style="margin-left: 10px;" type="button">Edit</button>
													</a>
												</div>
											</td>
										</tr>
									@endforeach
								@endisset
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- DataTables Example End Here -->
		</div>
	</div>
</main>
@endsection

@push('scripts')
<script src="{{ asset('datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/plugins/sweetalert.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#dataTable').DataTable({
			"aaSorting": [],
		});
	});
</script>
@endpush