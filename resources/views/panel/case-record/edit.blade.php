@php
	if(!empty($caseRecord->id)){
		$caseEncryptId = param_encrypt($caseRecord->id);
	}
@endphp

@extends('panel.layouts.app')

@section('title', 'Edit CaseRecord')

@section('content')
<main class="app-content">

	@include('panel.layouts.case-record-app-title-item')

	<div class="row">
		<div class="col-md-12">
			<div class="tile">
				<div class="tile-body">
					<form class="form-horizontal" method="POST" action="{{ route('case-record.update', $caseEncryptId) }}" enctype="multipart/form-data" autocomplete="off">
						@csrf
						@method('PUT')
						<div class="form-group row mt-2">
							<label class="control-label col-md-3">{{ __('Name') }}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $caseRecord->applicant_name }}" placeholder="Enter full name" required autofocus>

								@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Mobile Number') }}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{{ $errors->has('mobileNumber') ? ' is-invalid' : '' }}" name="mobileNumber" value="{{ $caseRecord->applicant_mobile }}" placeholder="Enter mobile number" required autofocus>

								@if ($errors->has('mobileNumber'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('mobileNumber') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Email') }}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $caseRecord->applicant_email }}" placeholder="Enter email" required autofocus>

								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Address') }}</label>
							<div class="col-md-8">
								<textarea type="text" rows="4" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="Enter applicant address" required autofocus>{{ $caseRecord->applicant_address }}</textarea>

								@if ($errors->has('address'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('address') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Case Type') }}</label>
							<div class="col-md-8">
								<select class="form-control{{ $errors->has('caseType') ? ' is-invalid' : '' }}" name="caseType" required autofocus>
									<option value="">Select Case Type</option>
									@foreach ($CaseType as $key => $value)   
										@if( $caseRecord->case_type == $value->id )
											<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
										@else
											<option value="{{ $value->id }}">{{ $value->name }}</option>
										@endif
									@endforeach
								</select>
								@if ($errors->has('caseType'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('caseType') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Remarks') }}</label>
							<div class="col-md-8">
								<textarea type="text" rows="4" class="form-control{{ $errors->has('remarks') ? ' is-invalid' : '' }}" name="remarks" placeholder="Enter remarks" autofocus>{{ $caseRecord->remarks }}</textarea>

								@if ($errors->has('remarks'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('remarks') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Priority') }}</label>
							<div class="col-md-8">
								<select class="form-control{{ $errors->has('priority') ? ' is-invalid' : '' }}" name="priority"  autofocus>
									<option value="">Select Priority</option>
									@foreach (constant('PRIORITY') as $key => $value)   
										@if( $caseRecord->priority == $key )
											<option value="{{ $key }}" selected>{{ $value }}</option>
										@else
											<option value="{{ $key }}">{{ $value }}</option>
										@endif
									@endforeach
								</select>
								@if ($errors->has('priority'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('priority') }}</strong>
									</span>
								@endif
							</div>
						</div>
						@if(Auth::User()->privilege())
							<div class="form-group row">
								<label class="control-label col-md-3">{{ __('Expected to close') }}</label>
								<div class="col-md-8">
									<input id="expected_to_close" class="form-control{{ $errors->has('expected_to_close') ? ' is-invalid' : '' }}" name="expected_to_close" type="text" placeholder="Select Date">
									@if ($errors->has('expected_to_close'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('expected_to_close') }}</strong>
										</span>
									@endif
								</div>
							</div>
						@endif
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Application from') }}</label>
							<div class="col-md-8">
								<select class="form-control{{ $errors->has('application_from') ? ' is-invalid' : '' }}" name="application_from"  autofocus>
									<option value="">Select Application From</option>
									@foreach (constant('APPLICATION_FROM') as $key => $value)   
										@if( $caseRecord->application_from == $key )
											<option value="{{ $key }}" selected>{{ $value }}</option>
										@else
											<option value="{{ $key }}">{{ $value }}</option>
										@endif
									@endforeach
								</select>
								@if ($errors->has('application_from'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('application_from') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Case transfer to') }}</label>
							<div class="col-md-8">
								<select class="form-control{{ $errors->has('case_tranfer_to') ? ' is-invalid' : '' }}" name="case_tranfer_to"  autofocus>
									<option value="">Select Case Transfer To</option>
									@foreach (constant('CASE_TRANSFER_TO') as $key => $value)   
										@if( $caseRecord->case_tranfer_to == $key )
											<option value="{{ $key }}" selected>{{ $value }}</option>
										@else
											<option value="{{ $key }}">{{ $value }}</option>
										@endif
									@endforeach
								</select>
								@if ($errors->has('case_tranfer_to'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('case_tranfer_to') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="tile-footer">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-primary m-2 float-right" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
									<button onclick="pageReload()" class="btn btn-secondary m-2 float-right" type="button"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@if(Auth::User()->privilege())
	@push('scripts')

	<script src="{{ asset('js/plugins/bootstrap-datepicker.min.js') }}"></script>
	<script src="{{ asset('js/plugins/select2.min.js') }}"></script>
	<script src="{{ asset('js/plugins/bootstrap-datepicker.min.js') }}"></script>

	<script type="text/javascript">

		$( document ).ready(function() {

			$('#expected_to_close').datepicker({
				format: "dd-mm-yyyy",
				autoclose: true,
				todayHighlight: true,
				date: "today",
				startDate: "today",
				// // endDate: "today",
			}).datepicker("setDate", "{{ date('d-m-Y', strtotime($caseRecord->expected_to_close)) }}");

		});

	</script>

	@endpush
@endif