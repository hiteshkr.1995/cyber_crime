@extends('panel.layouts.app')

@section('title', 'File Details')

@section('content')
<main class="app-content">

	@include('panel.layouts.case-record-app-title-item')

	<div class="row">
		<div class="col-md-12">
			<div class="tile">

				@if (count($UploadedFile) > 0)
					<table class="table table-bordered">
						<thead>
							<tr>
								<th scope="col">File Name</th>
								<th scope="col">File Type</th>
								<th scope="col">Created At</th>
								<th scope="col">Download</th>
								<th scope="col">Delete</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($UploadedFile as $key => $value)
								<tr>
									<td>{{ $value->original_name }}</td>
									<td>{{ define_option($value->option, 'name') ?? 'Not Define' }}</td>
									<td>{{ $value->created_at->format('d M Y h:i:s A') }}</td>
									<td>
										<center>
											<a target="_blank" href="{{ asset_storage($value->path.$value->name) }}">
												<button class="btn btn-sm btn-outline-info clearfix">
													<span class="fa fa-arrow-down"></span> Download
												</button>
											</a>
										</center>
									</td>
									<td>
										<center>
											<form id="delete-table-row-{{ param_encrypt($value->id) }}" action="{{ route('case-record.file.destroy', [param_encrypt($value->case_record_id),param_encrypt($value->id)]) }}" method="POST">
												@csrf
												@method('DELETE')
												<button onclick="deleteTableRow('{{ param_encrypt($value->id) }}', event)" class="btn-sm btn-outline-danger" type="button">Delete</button>
											</form>
										</center>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				@else
					<center>
						<h2>No record found.</h2>
					</center>
				@endif

			</div>

		</div>
	</div>
</main>
@endsection
@push('scripts')
<script src="{{ asset('js/plugins/sweetalert.min.js') }}"></script>
@endpush