@php
	if(!empty($caseRecord->id)){
		$caseEncryptId = param_encrypt($caseRecord->id);
	}
@endphp

@extends('panel.layouts.app')

@section('title', 'Preview CaseRecord')

@push('plugins')

<style type="text/css">
.table-scrolle {
	overflow: auto;
	max-height: 35%;
}
</style>

@endpush

@section('content')
<main class="app-content">
	
	@include('panel.layouts.case-record-app-title-item')

	<div class="row">
		<div class="col-md-12">

			<div class="tile">
				<div class="row">
					<div class="col-md-6">
						<div class="tile-body">
							<div class="table-responsive-1">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Data</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Case Id</td>
											<td>{{ $caseRecord->case_id ?? '' }}</td>
										</tr>
										<tr>
											<td>Name</td>
											<td>{{ $caseRecord->applicant_name ?? '' }}</td>
										</tr>
										<tr>
											<td>Mobile Number</td>
											<td>{{ $caseRecord->applicant_mobile ?? '' }}</td>
										</tr>
										<tr>
											<td>Email</td>
											<td>{{ $caseRecord->applicant_email ?? '' }}</td>
										</tr>
										<tr>
											<td>Address</td>
											<td>{{ $caseRecord->applicant_address ?? '' }}</td>
										</tr>
										<tr>
											<td>Case Status</td>
											<td>{{ act_inact($caseRecord->case_status) ?? '' }}</td>
										</tr>
										<tr>
											<td>Case Type</td>
											<td>{{ $caseRecord->caseType->name ?? '' }}</td>
										</tr>
										<tr>
											<td>Assigne To</td>
											<td>{{ $caseRecord->assignedTo->name ?? 'Not assigned' }}</td>
										</tr>
										<tr>
											<td>Created By</td>
											<td>{{ $caseRecord->createdBy->name ?? '' }}</td>
										</tr>
										<tr>
											<td>Created At</td>
											<td>{{ $caseRecord->created_at->format('d M Y h:i:s A') ?? '' }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						@if (Auth::user()->privilege())
							<div class="tile-body">

								<form class="form-horizontal" method="POST" action="{{ route('case-record.assign', $caseEncryptId) }}" enctype="multipart/form-data">
									@csrf
									<div class="form-group">
										<label>{{ __('Select user to assign') }}</label>
										<div class="row">
											<div class="col-md-9">
												<select class="form-control{{ $errors->has('user') ? ' is-invalid' : '' }} mt-2 mb-2" name="user" required>
													<option value="">Select User</option>
													@isset($user)
														@foreach ($user as $key => $value)
															@if( old('user') == $value->id )
																<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
															@else
																<option value="{{ $value->id }}">{{ $value->name }}</option>
															@endif
														@endforeach
													@endisset
												</select>
											</div>
											<div class="col-md-3">
												<button class="btn btn-info m-2 float-right" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Assign</button>
											</div>
										</div>
										@if ($errors->has('user'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('user') }}</strong>
											</span>
										@endif
									</div>
								</form>
							</div>
							<hr>
						@endif
						<h3 class="mt-5">
							{{ __('Assign History') }}
						</h3>
						<div class="table-scrolle">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th width="1%" scope="col">#</th>
										<th scope="col">Assign To</th>
										<th scope="col">Date and Time</th>
									</tr>
								</thead>
								<tbody>
									@if (!empty($caseRecord->history))
										@foreach(array_reverse(json_decode($caseRecord->history)) as $key => $value)
											<tr>
												<td>{{ ++$key }}</td>
												<td>{{ (!empty($value->assignTo))?user_name($value->assignTo):'' }}</td>
												<td>{{ (!empty($value->timestamp))?date('d M Y h:i:s A', $value->timestamp):'' }}</td>
											</tr>
										@endforeach
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection