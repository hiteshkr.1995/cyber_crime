@extends('panel.layouts.app')

@section('title', 'Users')

@push('plugins')
<link href="{{ asset('datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endpush

@section('content')
<main class="app-content">
	<div class="app-title">
		<div>
			<h1>Users</h1>
			<p>Details of all the Users</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- DataTables Example -->
			<div class="card mb-3">
				<div class="card-header">
					User Table
					<a class="btn btn-info float-right" href="{{ route('register') }}">Add User</a>
				</div>
				<div class="card-body col-md">
					<div class="table-responsive">
						<table id="dataTable" class="table table-bordered display" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Mobile Number</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@isset($user)
									@foreach ($user as $key => $value)
										<tr >
											<td>
												<a href="{{ route('user.show', param_encrypt($value->id)) }}">{{ $value->name }}</a>
											</td>
											<td>{{ $value->email }}</td>
											<td>{{ $value->mobile_number }}</td>
											<td>
												<center>
													{{ act_inact($value->status) }}
												</center>
											</td>
											<td width="15%">
												<div class="row justify-content-center">
													@if($value->id != Auth::user()->id)
														<form id="delete-table-row-{{ param_encrypt($value->id) }}" action="{{ route('user.destroy', param_encrypt($value->id)) }}" method="POST">
															@csrf
															@method('DELETE')
															<button onclick="deleteTableRow('{{ param_encrypt($value->id) }}', event)" class="btn-sm btn-outline-danger" type="button">Delete</button>
														</form>
													@endif
													<a href="{{ route('user.edit', param_encrypt($value->id)) }}">
														<button class="btn-sm btn-outline-info" style="margin-left: 10px;" type="button">Edit</button>
													</a>
												</div>
											</td>
										</tr>
									@endforeach
								@endisset
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- DataTables Example End Here -->
		</div>
	</div>
</main>
@endsection

@push('scripts')
<script src="{{ asset('datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/plugins/sweetalert.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#dataTable').DataTable({
			"aaSorting": [],
		});
	});
</script>
@endpush