@extends('panel.layouts.app')

@section('title', 'Edit User')

@section('content')
<main class="app-content">
	<div class="app-title">
		<div>
			<h1>Edit User</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="tile">
				<div class="tile-body">
					<form class="form-horizontal" method="POST" action="{{ route('user.update', param_encrypt($user->id)) }}" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-group row mt-2">
							<label class="control-label col-md-3">{{ __('Name') }}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" placeholder="Enter full name" required autofocus>

								@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Mobile Number') }}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{{ $errors->has('mobileNumber') ? ' is-invalid' : '' }}" name="mobileNumber" value="{{ $user->mobile_number }}" placeholder="Enter mobile number" required autofocus>

								@if ($errors->has('mobileNumber'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('mobileNumber') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Email') }}</label>
							<div class="col-md-8">
								<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" placeholder="Enter email" required autofocus>

								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
						    <label class="control-label col-md-3">{{ __('Designation') }}</label>
						    <div class="col-md-8">
						        <select class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" required autofocus {{ ($user->id === auth()->user()->id ? 'disabled' : '' ) }}>
						            <option value="">Select Designation</option>
						            @isset($designations)
							            @foreach ($designations as $key => $designation)
							                @if( old('designation') == $designation->id )
							                    <option value="{{ $designation->id }}" selected>{{ $designation->name }}</option>
							                @elseif( $designation->id == $user->designation )
							                    <option value="{{ $designation->id }}" selected>{{ $designation->name }}</option>
							                @else
							                    <option value="{{ $designation->id }}">{{ $designation->name }}</option>
							                @endif
							            @endforeach
							        @endisset
						        </select>
						        @if ($errors->has('designation'))
						            <span class="invalid-feedback" role="alert">
						                <strong>{{ $errors->first('designation') }}</strong>
						            </span>
						        @endif
						    </div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Status') }}</label>

							<div class="col-md-8">
								<div class="form-check-inline animated-radio-button">
									<label class="form-check-label">
										<input class="form-check-input" type="radio" name="status" value="1" {{ ($user->status == 1)? 'checked' : '' }} ><span class="label-text">Active</span>
									</label>
								</div>
								<div class="form-check-inline animated-radio-button">
									<label class="form-check-label">
										<input class="form-check-input" type="radio" name="status" value="0" {{ ($user->id === auth()->user()->id ? 'disabled' : '' ) }} {{ ($user->status == 0)? 'checked' : '' }} ><span class="label-text">Inactive</span>
									</label>
								</div>

								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>
						{{-- <div class="form-group row">
							<label class="control-label col-md-3">{{ __('Address') }}</label>
							<div class="col-md-8">
								<textarea type="text" rows="4" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="Enter applicant address" required autofocus>{{ $caseRecord->applicant_address }}</textarea>

								@if ($errors->has('address'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('address') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Case Type') }}</label>
							<div class="col-md-8">
								<select class="form-control{{ $errors->has('caseType') ? ' is-invalid' : '' }}" name="caseType" required autofocus>
									<option value="">Select Case Type</option>
									@foreach ($CaseType as $key => $value)   
										@if( $caseRecord->case_type == $value->id )
											<option value="{{ $value->id }}" selected>{{ $value->case_type }}</option>
										@else
											<option value="{{ $value->id }}">{{ $value->case_type }}</option>
										@endif
									@endforeach
								</select>
								@if ($errors->has('caseType'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('caseType') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<label class="control-label col-md-3">{{ __('Remarks') }}</label>
							<div class="col-md-8">
								<textarea type="text" rows="4" class="form-control{{ $errors->has('remarks') ? ' is-invalid' : '' }}" name="remarks" placeholder="Enter remarks" required autofocus>{{ $caseRecord->remarks }}</textarea>

								@if ($errors->has('remarks'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('remarks') }}</strong>
									</span>
								@endif
							</div>
						</div> --}}
						<div class="tile-footer">
							<div class="row">
								<div class="col-md-12">
									<button class="btn btn-primary m-2 float-right" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
									<button onclick="pageReload()" class="btn btn-secondary m-2 float-right" type="button"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection