@extends('panel.layouts.app')

@section('title', 'User Data')

@push('plugins')

<style type="text/css">
.table-responsive-2 {
	overflow: auto;
	max-height: 35%;
}
</style>

@endpush

@section('content')
<main class="app-content">
	<div class="app-title">
		<div class="col-2">
			<h1>User Data</h1>
		</div>
		<div class="col-8">
			@include('sharing.messages')
		</div>

		<div class="col-2">
			<a class="btn btn-info float-right" href="{{ route('user.edit', param_encrypt($user->id) ) }}">Edit User</a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">

			<div class="tile">
				<div class="row">
					@isset($user)
					<div class="col-md-12">
						<div class="tile-body">
							<div class="table-responsive-1">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th scope="col">#</th>
											<th scope="col">Data</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Id</td>
											<td>{{ $user->id }}</td>
										</tr>
										<tr>
											<td>Name</td>
											<td>{{ $user->name }}</td>
										</tr>
										<tr>
											<td>Mobile Number</td>
											<td>{{ $user->mobile_number }}</td>
										</tr>
										<tr>
											<td>Email</td>
											<td>{{ $user->email }}</td>
										</tr>
										<tr>
											<td>Case Status</td>
											<td>{{ act_inact($user->status) }}</td>
										</tr>
										<tr>
											<td>Created By</td>
											<td>{{ user_name($user->created_by) }}</td>
										</tr>
										<tr>
											<td>Created At</td>
											<td>{{ $user->created_at->format('d M Y h:i:s A') }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					@endisset
				</div>
			</div>

		</div>


	</div>
</main>
@endsection

@push('scripts')
	<script type="text/javascript">

		$('.alert').css('margin', '0px');

	</script>
@endpush