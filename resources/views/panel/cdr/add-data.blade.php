@extends('panel.layouts.app')

@section('title', 'Add CdrData')

@section('content')
<main class="app-content">

	@include('panel.layouts.case-record-app-title-item')

	<div class="row">
		<div class="col-md-12">

			<div class="tile">

				<form class="form-horizontal" method="POST" action="{{ route('cdr.data.submit', $caseEncryptId) }}" enctype="multipart/form-data">
					@csrf

					<div class="form-group row">
						<label class="control-label col-md-3">{{ __('Cdr Type') }}</label>
						<div class="col-md-8">
							<select class="form-control{{ $errors->has('cdr') ? ' is-invalid' : '' }}" name="cdr" required autofocus>
								<option value="">Select Cdr Type</option>
								@if (!empty($cdrType))
									@foreach ($cdrType as $key => $value)   
										@if( old('cdr') == $value->id )
											<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
										@else
											<option value="{{ $value->id }}">{{ $value->name }}</option>
										@endif
									@endforeach
								@endif
							</select>
							@if ($errors->has('cdr'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('cdr') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="form-group row">
						<label class="control-label col-md-3">Upload File</label>
						<div class="col-md-8">
							<input class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" type="file" name="file" required>
							@if ($errors->has('file'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('file') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="tile-footer">
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-primary m-2 float-right" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Upload</button>
								<button onclick="pageReload()" class="btn btn-secondary m-2 float-right" type="button"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</main>
@endsection