@extends('panel.layouts.app')

@section('title', 'View WalletData')

@section('content')
<main class="app-content">

	@include('panel.layouts.case-record-app-title-item')

	<div class="row">
		<div class="col-md-12">
			<div class="tile">
 
				<form class="form-horizontal" method="GET" action="{{ route('cdr.data.view', $caseEncryptId) }}" enctype="multipart/form-data">
					<div class="form-group row">
						<div class="col-md-8">
							<select class="form-control{{ $errors->has('cdr') ? ' is-invalid' : '' }} mt-2 mb-2" name="cdr" required>
								<option value="">Select Cdr Type</option>
								@if (!empty($cdrType))
									@foreach ($cdrType as $key => $value)   
										@if( old('cdr') == $value->id )
											<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
										@else
											<option value="{{ $value->id }}">{{ $value->name }}</option>
										@endif
									@endforeach
								@endif
							</select>
							@if ($errors->has('cdr'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('cdr') }}</strong>
								</span>
							@endif
						</div>
						<div class="col-md-4">
							<button class="btn btn-info m-2 float-right" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Show Data</button>
						</div>
					</div>
				</form>
			</div>

			@isset($dataArray)
				@foreach($dataArray as $key => $headers)
					<div class="tile">
						<div class="card-body">
								<div class="table-responsive">
									<table id="table-{{ ++$key }}" class="table table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>#</th>
												@foreach($headers as $head => $value)
													<th>{{ ucfirst($value) }}</th>
												@endforeach
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
						</div>
					</div>
				@endforeach
			@endisset

		</div>
	</div>
</main>
@endsection

@push('scripts')
<script src="{{ asset('datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/plugins/sweetalert.min.js') }}"></script>

<script type="text/javascript">
	@if (!empty($dataArray))

	// var table; 

		// $(document).ready(function() {
			@foreach($dataArray as $key => $headers)
				$('#table-{{ ++$key }}').DataTable({
					"aaSorting": [],
					"processing": true,
					"serverSide": true,
					// "bStateSave": true,
					"ajax": {
						"url": "{{ route('case-record.types-table-data', $caseEncryptId) }}",
						"type": "GET",
						"data": {
							"value1": "option2",
							"value2": "{{ request()->cdr }}",
							"value3": "{{ --$key }}",
						},
					},
					"columns": [
						{ "data": "#", "name": "#" },
						@foreach ($headers as $head => $value)
							{ "data": "{{ $value }}", "name": "{{ $value }}" },
						@endforeach
					],
					"columnDefs": [
						{ "orderable": false, "targets": 0 },
						{ "searchable": false, "targets": 0 },
					]
				});
			@endforeach
		// });

		$.fn.dataTable.ext.errMode = 'none';

	@endif
</script>
@endpush