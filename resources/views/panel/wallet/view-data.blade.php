@extends('panel.layouts.app')

@section('title', 'View Record')

@section('content')
<main class="app-content">

	@include('panel.layouts.case-record-app-title-item')

	<div class="row">
		<div class="col-md-12">
			<div class="tile">
 
				<form class="form-horizontal" method="GET" action="{{ route('wallet.data.view', $caseEncryptId) }}" enctype="multipart/form-data">
					<div class="form-group row">
						<div class="col-md-8">
							<select class="form-control{{ $errors->has('wallet') ? ' is-invalid' : '' }} mt-2 mb-2" name="wallet" required>
								<option value="">Select Wallet Type</option>
								@if (!empty($walletType))
									@foreach ($walletType as $key => $value)   
										@if( old('wallet') == $value->id )
											<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
										@else
											<option value="{{ $value->id }}">{{ $value->name }}</option>
										@endif
									@endforeach
								@endif
							</select>
							@if ($errors->has('wallet'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('wallet') }}</strong>
								</span>
							@endif
						</div>
						<div class="col-md-4">
							<button class="btn btn-info m-2 float-right" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Show Data</button>
						</div>
					</div>
				</form>
			</div>

			@isset($dataArray)
				@foreach($dataArray as $key => $headers)
					<div class="tile">
						<div class="card-body">
								<div class="table-responsive">
									<table id="table-{{ ++$key }}" class="table table-bordered" width="100%" cellspacing="0">
										<thead>
											<tr>
												<th>#</th>
												@foreach($headers as $head => $value)
													<th>{{ ucfirst($value) }}</th>
												@endforeach
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
						</div>
					</div>
				@endforeach
			@endisset

		</div>
	</div>
</main>
@endsection

@push('scripts')
<script src="{{ asset('datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('datatables/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/plugins/sweetalert.min.js') }}"></script>

<script type="text/javascript">

	// var tables = '';
	@if (!empty($dataArray))

		// $(document).ready(function() {
			@foreach($dataArray as $key => $headers)
				$('#table-{{ ++$key }}').DataTable({
					"aaSorting": [],
					"processing": true,
					"serverSide": true,
					"ajax": {
						"url": "{{ route('case-record.types-table-data', $caseEncryptId) }}",
						"type": "GET",
						"data": {
							"value1": "option1",
							"value2": "{{ request()->wallet }}",
							"value3": "{{ --$key }}",
						},
					},
					"columns": [
						{ "data": "#", "name": "#" },
						@foreach ($headers as $head => $value)
							{ "data": "{{ $value }}", "name": "{{ $value }}" },
						@endforeach
					],
					"columnDefs": [
						{ "orderable": false, "targets": 0 },
						{ "searchable": false, "targets": 0 },
					],
					// "pageLength": 100,
					// "lengthMenu": [
					// 	[5, 10, 25, 50, 100],
					// 	[5, 10, 25, 50, 100]
					// ],
				});
			@endforeach
		// });

		$.fn.dataTable.ext.errMode = 'none';

	@endif

</script>
@endpush