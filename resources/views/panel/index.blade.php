@extends('panel.layouts.app') 

@section('title', 'Dashboard')

@section('content') 
<main class="app-content">
	<div class="app-title">
		<div>
			<h1>Dashboard</h1>
			<p>Welcome to the Dashboard</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-lg-4">
			<div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
				<div class="info">
					<h4>Cases</h4>
					<!-- Print the total number of the cases -->
					<p><b>{{ $totalCase }}</b></p>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-lg-4">
			<div class="widget-small info coloured-icon"><i class="icon fa fa-thumbs-o-up fa-3x"></i>
				<div class="info">
					<h4>Completed</h4>
					<!-- Print the total number of the completed cases  -->
					<p><b>{{ $completedCase }}</b></p>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-lg-4">
			<div class="widget-small warning coloured-icon"><i class="icon fa fa-files-o fa-3x"></i>
				<div class="info">
					<h4>Pending</h4>
					<!-- Print the total number of the pending cases  -->
					<p><b>{{ $pendingCase }}</b></p>
				</div>
			</div>
		</div>
		 
		</div>
	</div>
	{{--<div class="row">
		 
		<div class="col-md-6">
			<div class="tile">
				<h3 class="tile-title">Case Assigned</h3>
				<div class="embed-responsive embed-responsive-16by9">
					<canvas class="embed-responsive-item" id="pieChartDemo"></canvas>
				</div>
			</div>
		</div>

		 <div class="col-md-6">
			<div class="tile">
				<h3 class="tile-title">Case Graph</h3>
				<div class="embed-responsive embed-responsive-16by9">
					<canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
				</div>
			</div>
		</div>  

	</div>--}}
	 
</main>
@endsection

@push('scripts')
<!-- Page specific javascripts-->
<script type="text/javascript" src="{{ asset('js/plugins/chart.js') }}"></script>
<script type="text/javascript">
	var data = {
		labels: ["January", "February", "March", "April", "May"],
		datasets: [
			{
				label: "My First dataset",
				fillColor: "rgba(220,220,220,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: [65, 59, 80, 81, 56]
			},
			{
				label: "My Second dataset",
				fillColor: "rgba(151,187,205,0.2)",
				strokeColor: "rgba(151,187,205,1)",
				pointColor: "rgba(151,187,205,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(151,187,205,1)",
				data: [28, 48, 40, 19, 86]
			}
		]
	};
	//get the data from the database print here
	var user_data = [
		{
			value: 300,
			color: "#46BFBD",
			highlight: "#5AD3D1",
			label: "Complete"
		},
		{
			value: 50,
			color:"#F7464A",
			highlight: "#FF5A5E",
			label: "In-Progress"
		}
	]
	
	var ctxl = $("#lineChartDemo").get(0).getContext("2d");
	var lineChart = new Chart(ctxl).Line(data);
	
	var ctxp = $("#pieChartDemo").get(0).getContext("2d");
	var user_chart = new Chart(ctxp).Pie(user_data);
</script>
@endpush