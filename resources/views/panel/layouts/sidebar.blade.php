<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar" style="padding-top: 50px;">
	<div class="app-sidebar__user" style="margin-bottom: 0;"><img class="app-sidebar__user-avatar" src="{{ asset('storage/img/profile.png') }}">
		<div>
			<p class="app-sidebar__user-name">{{ Auth::user()->name }}</p>
			<p class="app-sidebar__user-designation">{{ designation_name(Auth::user()->designation) }}</p>
		</div>
	</div>
	<ul class="app-menu">
		<li>
			<a class="app-menu__item @if(Route::is('dashboard')) active @endif" href="{{ route('dashboard') }}">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path d="M0 0h24v24H0z" fill="none"/>
					<path fill="#FFF" d="M3 13h8V3H3v10zm0 8h8v-6H3v6zm10 0h8V11h-8v10zm0-18v6h8V3h-8z"/>
				</svg>
				<span class="app-menu__label">Dashboard</span>
			</a>
		</li>
		@if(auth()->user()->isDcp())
			<li>
				<a class="app-menu__item @if(Route::is('user.index')) active @endif" href="{{ route('user.index') }}">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
						<path d="M0 0h24v24H0z" fill="none"/>
						<path fill="#FFF" d="M16 11c1.66 0 2.99-1.34 2.99-3S17.66 5 16 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm-8 0c1.66 0 2.99-1.34 2.99-3S9.66 5 8 5C6.34 5 5 6.34 5 8s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V19h14v-2.5c0-2.33-4.67-3.5-7-3.5zm8 0c-.29 0-.62.02-.97.05 1.16.84 1.97 1.97 1.97 3.45V19h6v-2.5c0-2.33-4.67-3.5-7-3.5z"/>
					</svg>
					<span class="app-menu__label">Users</span>
				</a>
			</li>
		@endif
		<li>
			<a class="app-menu__item @if(Route::is('case-record.index')) active @endif" href="{{ route('case-record.index') }}">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path fill="none" d="M0 0h24v24H0V0z"/>
					<path fill="#FFF" d="M3 15h8v-2H3v2zm0 4h8v-2H3v2zm0-8h8V9H3v2zm0-6v2h8V5H3zm10 0h8v14h-8V5z"/>
				</svg>
				<span class="app-menu__label">CaseRecord</span>
			</a>
		</li>
		{{--<li>
			<a class="app-menu__item @if(Route::is('case-record.imei-seach')) active @endif" href="{{ route('case-record.imei-seach') }}">
				 <svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="5 -350 700 1" preserveAspectRatio="xMidYMid meet">
				<g transform="translate(0,0) scale(0.1,-0.1)" fill="#FFF" stroke="none">
				<path d="M193 5853 l-193 -3 0 -2353 0 -2353 244 1 c241 0 245 1 269 23 54 51
				52 -59 52 2347 0 2473 5 2292 -65 2327 -20 10 -53 17 -75 16 -22 -2 -127 -4
				-232 -5z"></path>
				<path d="M1624 5850 c-66 -7 -101 -31 -127 -88 -21 -47 -21 -49 -21 -1534 l-1
				-1487 21 -28 c44 -59 74 -68 241 -71 145 -4 155 -3 207 21 36 17 59 35 68 54
				12 23 14 265 13 1493 -1 1342 -3 1470 -18 1520 -33 107 -61 123 -216 124 -64
				1 -139 -1 -167 -4z"></path>
				<path d="M2550 5850 c-126 -6 -142 -11 -164 -44 -38 -59 -38 -64 -37 -1590 2
				-1629 -3 -1515 64 -1557 30 -19 45 -20 222 -16 119 2 202 9 223 17 67 25 62
				-83 62 1550 0 997 -4 1496 -11 1528 -14 63 -32 88 -71 100 -47 14 -155 19
				-288 12z"></path>
				<path d="M3300 5848 c-25 -9 -41 -25 -55 -53 -20 -39 -20 -63 -23 -1550 -2
				-1425 -1 -1512 15 -1545 23 -45 54 -60 129 -60 73 0 108 22 124 78 8 26 10
				488 8 1537 -3 1348 -5 1504 -19 1535 -9 19 -23 41 -31 48 -23 18 -107 24 -148
				10z"></path>
				<path d="M4159 5851 c-321 -6 -314 -5 -347 -65 -16 -29 -17 -147 -20 -1546 -2
				-1488 -2 -1516 17 -1547 14 -21 32 -35 58 -42 23 -7 164 -11 356 -11 353 0
				371 3 407 62 20 33 20 42 17 1533 -2 1439 -3 1502 -21 1543 -22 50 -57 72
				-124 76 -26 1 -180 0 -343 -3z"></path>
				<path d="M6235 5852 c-53 -19 -72 -44 -85 -113 -14 -74 -14 -4393 0 -4467 12
				-64 42 -110 77 -118 33 -9 23 -9 431 -6 l342 2 0 2350 0 2350 -314 0 c-172 0
				-340 2 -372 5 -32 2 -68 1 -79 -3z"></path>
				<path d="M943 5835 c-47 -20 -62 -52 -74 -151 -15 -127 -1 -4407 15 -4454 17
				-52 57 -75 128 -75 54 0 62 3 88 32 17 19 32 52 39 85 8 38 11 676 9 2263 -3
				2142 -4 2211 -22 2248 -31 63 -108 85 -183 52z"></path>
				<path d="M5060 5832 c-24 -12 -43 -33 -60 -67 l-25 -50 -2 -1484 c-2 -1635 -6
				-1521 60 -1561 20 -13 50 -20 80 -20 40 0 53 5 83 33 22 20 38 47 44 72 13 55
				12 2816 -1 2920 -13 99 -31 139 -74 159 -42 20 -62 20 -105 -2z"></path>
				<path d="M5636 5835 c-39 -21 -52 -40 -71 -108 -15 -53 -16 -190 -14 -1515 1
				-1169 4 -1463 15 -1489 40 -101 227 -94 261 9 9 28 12 387 10 1515 -2 1456 -2
				1479 -22 1517 -11 22 -32 48 -46 59 -36 26 -97 32 -133 12z"></path>
				<path d="M4260 2017 c-73 -17 -129 -46 -154 -80 -21 -28 -21 -41 -24 -593 -2
				-377 1 -578 8 -605 14 -51 64 -108 115 -130 35 -15 79 -18 330 -20 325 -3 321
				-4 355 66 42 89 33 142 -30 179 -30 18 -59 21 -205 27 -138 5 -177 9 -208 25
				-67 32 -97 127 -62 193 25 50 78 63 273 69 196 5 204 8 242 87 26 54 28 96 6
				129 -25 39 -64 54 -168 65 -129 13 -294 63 -333 102 -25 25 -30 37 -30 78 0
				99 63 131 282 141 128 7 158 11 185 28 46 29 82 93 73 133 -10 44 -61 97 -103
				108 -56 14 -487 12 -552 -2z"></path>
				<path d="M1830 1972 c-30 -31 -54 -67 -62 -94 -17 -56 -16 -1051 1 -1113 6
				-22 25 -65 43 -95 59 -99 107 -91 183 31 l40 64 2 545 c1 309 -3 558 -8 575
				-11 36 -93 120 -127 130 -20 6 -32 -2 -72 -43z"></path>
				<path d="M2412 1985 c-16 -14 -38 -44 -48 -67 -17 -40 -18 -81 -17 -593 1
				-536 2 -551 22 -607 31 -81 55 -108 97 -108 28 0 45 10 85 47 86 80 83 68 89
				411 l5 304 28 24 c50 43 98 25 156 -59 19 -29 53 -56 105 -86 66 -38 82 -43
				120 -39 80 9 153 52 253 149 104 100 128 108 167 56 20 -27 21 -43 26 -355 5
				-288 8 -332 24 -370 26 -60 58 -82 118 -82 63 0 83 11 110 63 23 42 23 42 26
				622 2 393 0 592 -8 617 -16 55 -61 88 -121 88 -27 0 -65 -7 -85 -15 -33 -13
				-128 -97 -318 -276 -89 -84 -147 -119 -199 -119 -28 0 -50 13 -108 61 -41 34
				-102 89 -136 123 -177 176 -177 176 -253 206 -92 36 -101 36 -138 5z"></path>
				<path d="M5341 1984 c-17 -14 -44 -47 -59 -73 l-27 -46 1 -540 1 -540 31 -66
				c37 -78 73 -119 103 -119 29 0 87 62 122 132 l28 56 -1 523 c-1 318 -6 540
				-12 564 -13 52 -94 135 -131 135 -14 0 -39 -12 -56 -26z"></path>
				</g>
				</svg>
				<span class="app-menu__label">Search Imei</span>
			</a>
		</li>--}}
		{{-- <li class="treeview">
			<a class="app-menu__item" href="" data-toggle="treeview">
				<i class="app-menu__icon fa fa-laptop"></i>
				<span class="app-menu__label">Example</span>
				<i class="treeview-indicator fa fa-angle-right"></i>
			</a>
			<ul class="treeview-menu">
				<li>
					<a class="treeview-item" href="bootstrap-components">
						<i class="icon fa fa-circle-o"></i> Bootstrap Elements
					</a>
				</li>
				<li>
					<a class="treeview-item" href="bootstrap-components">
						<i class="icon fa fa-circle-o"></i> Bootstrap Elements
					</a>
				</li>
				<li>
					<a class="treeview-item" href="bootstrap-components">
						<i class="icon fa fa-circle-o"></i> Bootstrap Elements
					</a>
				</li>
			</ul>
		</li> --}}
	</ul>
</aside>