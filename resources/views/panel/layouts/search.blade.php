<div class="modal-search modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div style="background-color: rgba(225, 225, 225, 0.5);" class="close-modal" data-dismiss="modal">
					<div class="lr">
						<div class="rl"></div>
					</div>
				</div>
				<div class="container">
					<form action="" method="GET" id="site_search">
						<div class="input-group">
							<input style="background-color: rgba(225, 225, 225, 0.8);" class="form-control" type="text" id="search_box" name="value" placeholder="Search..." autocomplete="off" autofocus>
							<span style="background-color: rgba(225, 225, 225, 0.5);" class="input-group-btn">
								<button class="btn" type="submit">
									<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
										<path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
										<path d="M0 0h24v24H0z" fill="none"/>
									</svg>
									<!-- <i class="fa fa-search"></i> -->
								</button>
							</span>
						</div>
					</form>
					<ul class="d-block mt-4" id="search_results"></ul>
				</div>
			</div>
		</div>
	</div>
</div>