<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="icon" href="{{ asset('favicon.ico') }}" type="ico">
		<title>@yield('title') | {{ config('app.name') }}</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<!-- Main CSS-->
		<link rel="stylesheet" href="{{ asset('css/search.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
		<!-- Font-icon css-->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
		{{-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> --}}
		@stack('plugins')
	</head>
	<body class="app sidebar-mini rtl">

		<!-- Navbar-->
		@include('panel.layouts.navbar')

		<!-- Sidebar menu-->
		@include('panel.layouts.sidebar')

		<!-- content-->
		@yield('content')

		{{--<!-- Search modal -->
		@include('panel.layouts.search')--}}

		<!-- Essential javascripts for application to work-->
		<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
		<script src="{{ asset('js/popper.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/main.js') }}"></script>
		<!-- The javascript plugin to display page loading on top-->
		<script src="{{ asset('js/plugins/pace.min.js') }}"></script>
		<script type="text/javascript">
			var BASE_URL = "{{ url('') }}";
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
		</script>

		@stack('scripts')

	</body>
</html>