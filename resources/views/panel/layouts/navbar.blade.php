<header class="app-header"><a class="app-header__logo" href="{{ route('dashboard') }}">CyberCell</a>
	<!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
	<!-- Navbar Right Menu-->
	<ul class="app-nav">
		 
		<!--Notification Menu-->
	 
		<!-- User Menu-->
		{{--@if(auth()->user()->designation == 2)
			<li>
				<a class="app-nav__item" style="background-color: #17a2b8;" href="{{ route('register') }}">
					Register User
				</a>
			</li>
		@endif
		<li>
			<a href="#" class="app-nav__item" data-toggle="modal" data-target="#searchModal" onclick="">
				<svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
					<path fill=#FFF d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/>
					<path d="M0 0h24v24H0z" fill="none"/>
				</svg>
			</a>
		</li>--}}
		<li class="dropdown">
			<a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu">
				<svg width="40" height="40" viewBox="0 0 24 24">
					<path fill="#FFF" d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/>
					<path d="M0 0h24v24H0z" fill="none"/>
				</svg>
			</a>
			<ul class="dropdown-menu settings-menu dropdown-menu-right">
				{{--<li>
					<a style="font-size: 1.2em;" class="dropdown-item" href=""><i class="fa fa-cog fa-lg"></i> Settings</a>
				</li>
				<li>
					<a style="font-size: 1.2em;" class="dropdown-item" href=""><i class="fa fa-user fa-lg"></i> Profile</a>
				</li>--}}
				<li>
					<a style="font-size: 1.2em;" class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-lg"></i>{{ __('Logout') }}</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
				</li>
			</ul>
		</li>
	</ul>
</header>