@php
	use App\Models\CaseRecord;

	$caseId = param_decrypt($caseEncryptId);
	$caseRecord = CaseRecord::find($caseId);

	$caseId = $caseRecord->case_id;

@endphp
<div class="app-title">
	<div class="app-title-item">
		
		@if(Route::is('case-record.show'))

			<h1>Case Record: <span class="text-primary">{{ $caseId }}</span></h1>

		@elseif(Route::is('wallet.data.add'))

			<h1>Add WalletData: <span class="text-primary">{{ $caseId }}</span></h1>

		@elseif(Route::is('wallet.data.view'))

			<h1>View WalletData: <span class="text-primary">{{ $caseId }}</span></h1>

		@elseif(Route::is('case-record.edit'))

			<h1>Edit CaseRecord: <span class="text-primary">{{ $caseId }}</span></h1>

		@elseif(Route::is('case-record.files'))

			<h1>Files: <span class="text-primary">{{ $caseId }}</span></h1>

		@elseif(Route::is('cdr.data.add'))

			<h1>Add CdrData: <span class="text-primary">{{ $caseId }}</span></h1>

		@elseif(Route::is('cdr.data.view'))

			<h1>View CdrData: <span class="text-primary">{{ $caseId }}</span></h1>

		@endif

	</div>

	<div class="app-title-item">
		<ul class="nav nav-pills">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Add Data</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="{{ route('wallet.data.add', $caseEncryptId) }}">Wallet Data</a>
					<a class="dropdown-item" href="{{ route('cdr.data.add', $caseEncryptId) }}">CDR Data</a>
					<a class="dropdown-item" href="#">ISP Data</a>
				</div>
			</li>

			{{--<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">View Data</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="{{ route('wallet.data.view', $caseEncryptId) }}">Wallet Data</a>
					<a class="dropdown-item" href="{{ route('cdr.data.view', $caseEncryptId) }}">CDR Data</a>
					<a class="dropdown-item" href="#">ISP Data</a>
				</div>
			</li>--}}
		</ul>
	</div>

	@if(Route::is('case-record.show'))
		<div class="app-title-item">
			<a class="btn btn-info" style="margin: 2px;" href="{{ route('case-record.edit', $caseEncryptId) }}">Edit CaseRecord</a>
		</div>
	@endif

	<div class="app-title-item">
		<a class="btn btn-secondary" style="margin: 2px;" href="{{ route('case-record.files', $caseEncryptId) }}">Files</a>
	</div>

</div>
@include('sharing.messages')