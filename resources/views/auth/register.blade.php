@extends('auth.app')

@section('title', 'Registration')

@section('content')
<section class="material-half-bg">
    <div class="cover"></div>
</section>
<section class="lockscreen-content">
    <div class="logo">
        <h1></h1>
    </div>
    <div class="lock-box col-md-6">
        <h4 class="text-center">{{ __('Register User') }}</h4><hr>
        <form class="form-horizontal" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
                <label class="control-label col-md-3">{{ __('Name') }}</label>
                <div class="col-md-8">
                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Enter full name" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-md-3">{{ __('E-Mail Address') }}</label>
                <div class="col-md-8">
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter email address" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-md-3">{{ __('Mobile Number') }}</label>
                <div class="col-md-8">
                    <input type="text" class="form-control{{ $errors->has('mobileNumber') ? ' is-invalid' : '' }}" name="mobileNumber" value="{{ old('mobileNumber') }}" placeholder="Enter mobile number" required autofocus>

                    @if ($errors->has('mobileNumber'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mobileNumber') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-md-3">{{ __('Designation') }}</label>
                <div class="col-md-8">
                    <select class="form-control{{ $errors->has('designation') ? ' is-invalid' : '' }}" name="designation" required autofocus>
                        <option value="">Select Designation</option>
                        @isset($designations)
                            @foreach ($designations as $key => $designation)   
                                @if( old('designation') == $designation->id )
                                    <option value="{{ $designation->id }}" selected>{{ $designation->name }}</option>
                                @else
                                    <option value="{{ $designation->id }}">{{ $designation->name }}</option>
                                @endif
                            @endforeach
                        @endisset
                    </select>
                    @if ($errors->has('designation'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('designation') }}</strong>
                        </span>
                    @endif
                </div>
            </div><hr>
            <div class="form-group row float-right">
                <a class="btn btn-secondary m-2" href="{{ route('user.index') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>{{ __('Users') }}</a>
                <button class="btn btn-primary m-2" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>{{ __('Register') }}</button>
            </div>
        </form>
    </div>
</section>
@endsection