@extends('auth.app')

@section('title', 'Login')

@section('content')
<section class="material-half-bg">
	<div class="cover"></div>
</section>
<section class="lockscreen-content">
	<div >
		<center>
			<h2 class="login-head" style="color: #FFF"> Ahmedabad Cyber Cell Online Reporting and Documentation</h2> <!-- <img src="{{ url('storage/img/cyber_cell_logo.png') }}"> -->
		</center>
	</div>
	<div class="lock-box col-md-6">
		<form method="POST" class="login-form" action="{{ route('login') }}">
			@csrf
			<h3 class="text-center"><i class="fa fa-lg fa-fw fa-user"></i>{{ __('SIGN IN') }}</h3>
			<div class="form-group">
				<label class="control-label">{{ __('E-Mail Address') }}</label>
				<input id="email" style="font-size: 1.5rem;" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
				@if ($errors->has('email'))
					<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('email') }}</strong>
					</span>
				@endif
			</div>
			<div class="form-group">
				<label class="control-label">{{ __('Password') }}</label>
				<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

				@if ($errors->has('password'))
						<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
						</span>
				@endif
			</div>
			<div class="form-group">
				<div class="utility">
					<div class="animated-checkbox">
						<label>
							<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}><span class="label-text">{{ __('Stay Signed in') }}</span>
						</label>
					</div>
					<a class="semibold-text mb-2" href="{{ route('password.request') }}">
					    {{ __('Forgot Password?') }}
					</a>
					<!-- <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Forgot Password ?</a></p> -->
				</div>
			</div>
			<div class="form-group btn-container">
				<button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
			</div>
		</form>
	</div>
</section>
<!-- Essential javascripts for application to work-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="js/plugins/pace.min.js"></script>
@endsection