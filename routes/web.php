<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Middleware for auth
Route::middleware(['auth'])->group(function () {

	// Dashboard
	Route::get('/', 'Panel\DashboardController@index')->name('dashboard');

	// Users
	Route::get('/user', 'Panel\UserController@index')->name('user.index');
	Route::get('/user/{userEncryptId}/edit', 'Panel\UserController@edit')->name('user.edit');
	Route::get('/user/{userEncryptId}', 'Panel\UserController@show')->name('user.show');
	Route::put('/user/{userEncryptId}', 'Panel\UserController@update')->name('user.update');
	Route::delete('/user/{userEncryptId}', 'Panel\UserController@destroy')->name('user.destroy');

	// Caserecord
	// Route::get('/case-record/search/imei', 'Panel\CaseRecordController@searchImei')->name('case-record.imei-seach');
	Route::resource('case-record', 'Panel\CaseRecordController')->parameters(['case-record' => 'caseEncryptId']);
	Route::post('/case-record/{caseEncryptId}/assign', 'Panel\CaseRecordController@assignCaseRecord')->name('case-record.assign');
	Route::get('/case-record/{caseEncryptId}/pdf', 'Panel\CaseRecordController@pdfCaseRecord')->name('case-record.pdf');
	Route::get('/case-record/{caseEncryptId}/files', 'Panel\CaseRecordController@files')->name('case-record.files');
	Route::delete('/case-record/{caseEncryptId}/file/{uploadedFileEncryptId}', 'Panel\CaseRecordController@fileDestroy')->name('case-record.file.destroy');
	Route::get('/case-record/{caseEncryptId}/types-table-data', 'Panel\CaseRecordController@typesTableData')->name('case-record.types-table-data');
	Route::post('/case-record/{caseEncryptId}/is-malicious', 'Panel\CaseRecordController@isMalicious')->name('case-record.is-malicious');

	// Wallet
	Route::get('/wallet-data/{caseEncryptId}/add', 'Panel\WalletController@addData')->name('wallet.data.add');
	Route::post('/wallet-data/{caseEncryptId}/add', 'Panel\WalletController@uploadFile')->name('wallet.data.submit');
	// Route::get('/wallet-data/{caseEncryptId}/view', 'Panel\WalletController@viewData')->name('wallet.data.view');

	// Cdr
	Route::get('/cdr-data/{caseEncryptId}/add', 'Panel\CdrController@addData')->name('cdr.data.add');
	Route::post('/cdr-data/{caseEncryptId}/add', 'Panel\CdrController@uploadFile')->name('cdr.data.submit');
	// Route::get('/cdr-data/{caseEncryptId}/view', 'Panel\CdrController@viewData')->name('cdr.data.view');
	// Route::post('/cdr-data/{caseEncryptId}/is-malicious', 'Panel\CdrController@isMalicious')->name('cdr.data.is-malicious');


	// Resource Routes
	// Route::resources([
	// 	'case-record' => 'Panel\CaseRecordController',
	// ]);
});

// Temporery close
// Route::get('/home', 'HomeController@index')->name('home');
// 








// Define Global Variables

define('PRIORITY', 
	array_combine(range(1, count(['High', 'Medium', 'Low'])), array_values(['High', 'Medium', 'Low']))
);

define('APPLICATION_FROM', 
	array_combine(range(1, count(['Spl CD Crime', 'CP Ahmedabad', 'Govt DC', 'Other'])), array_values(['Spl CD Crime', 'CP Ahmedabad', 'Govt DC', 'Other']))
);

define('CASE_TRANSFER_TO', 
	array_combine(range(1, count(['Police Station', 'CBI', 'Other'])), array_values(['Police Station', 'CBI', 'Other']))
);

define('MODEL_PATH', 'App\\Models\\');

define('MODELS', [
	'model1' => 'AirtelMoney',
	'model2' => 'BillDesk',
	'model3' => 'CcAvenue',
	'model4' => 'CdrAirtel',
	'model5' => 'CdrBSNL',
	'model6' => 'CdrIdea',
	'model7' => 'CdrJio',
	'model8' => 'CdrTelenor',
	'model9' => 'CdrVodafone',
	'model10' => 'Flipkart',
	'model11' => 'Freecharge',
	'model12' => 'MPesa',
	'model13' => 'PaytmTxn',
	'model14' => 'PaytmTxnHistory',
	'model15' => 'PaytmTxnPerson',
]);

define('OPTION', [
	'option1' => 'WalletType',
	'option2' => 'CdrType',
]);


// Algorith for getting all models
// $path = app_path() . "/Models";

// function getModels($path){
// 	$out = [];
// 	$results = scandir($path);
// 	foreach ($results as $result) {
// 		if ($result === '.' or $result === '..') continue;
// 		$filename = $path . '/' . $result;
// 		if (is_dir($filename)) {
// 			$out = array_merge($out, getModels($filename));
// 		}else{
// 			$out[] = ucfirst(substr($filename,26,-4));
// 		}
// 	}
// 	return $out;
// }

// dd(getModels($path));