<?php

use Illuminate\Database\Seeder;

use App\Models\CdrType;

class CdrTypeTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if (count(CdrType::all()) == 0) {
			$data = array(
				array('name' => 'Airtel', 'model_name' => 'CdrAirtel'),
				array('name' => 'BSNL', 'model_name' => 'CdrBSNL'),
				array('name' => 'Idea', 'model_name' => 'CdrIdea'),
				array('name' => 'Jio', 'model_name' => 'CdrJio'),
				array('name' => 'Telenor', 'model_name' => 'CdrTelenor'),
				array('name' => 'Vodafone', 'model_name' => 'CdrVodafone'),
			);

			CdrType::insert($data);
		}
	}
}
