<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$email = 'dcp1@mail.com';

		if (!User::where('email', $email)->first()) {

			$data = array(
				'username' => 'DCP_1',
				'password' => Hash::make('password'),
				'email' => $email,
				'name' => 'Static Name',
				'mobile_number' => '1254879865',
				'designation' => '2',
				'privilege' => '1',
				'status' => '1',
				'created_by' => '1',
				'updated_by' => '1',
			);

			User::create($data);

		}
	}
}
