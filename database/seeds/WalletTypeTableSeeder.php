<?php

use Illuminate\Database\Seeder;
use App\Models\WalletType;

class WalletTypeTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if (count(WalletType::all()) == 0) {
			$data = array(
				array('name' => 'Paytm', 'model_name' => 'PaytmTxn,PaytmTxnHistory,PaytmTxnPerson'),
				array('name' => 'Airtel Money', 'model_name' => 'AirtelMoney'),
				array('name' => 'Bill Desk', 'model_name' => 'BillDesk'),
				array('name' => 'CC Avenue', 'model_name' => 'CcAvenue'),
				array('name' => 'Flipkart', 'model_name' => 'Flipkart'),
				array('name' => 'Freecharge', 'model_name' => 'Freecharge'),
				array('name' => 'M Pesa', 'model_name' => 'MPesa'),
			);

			WalletType::insert($data);
		}
	}
}
