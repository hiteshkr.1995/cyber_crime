<?php

use Illuminate\Database\Seeder;

use App\Models\CaseType;

class CaseTypeTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if (count(CaseType::all()) == 0) {
			$data = array(
				array('name' => 'E wallet'),
				array('name' => 'Otp/without otp'),
				array('name' => 'Social media'),
				array('name' => 'Metromonial fruad'),
				array('name' => 'Job fraud'),
				array('name' => 'Lottery /prize'),
				array('name' => 'Data theft'),
				array('name' => 'Website hack'),
			);

			CaseType::insert($data);
		}
	}
}
