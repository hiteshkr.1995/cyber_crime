<?php

use Illuminate\Database\Seeder;

use App\Models\Designation;

class DesignationTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if (count(Designation::all()) == 0) {
			$data = array(
				array('name' => 'ACP'),
				array('name' => 'DCP'),
				array('name' => 'PSI'),
				array('name' => 'HC'),
				array('name' => 'CP'),
			);

			Designation::insert($data);
		}
	}
}
