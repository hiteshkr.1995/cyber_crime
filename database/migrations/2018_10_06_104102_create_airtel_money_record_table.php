<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirtelMoneyRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airtel_money_record', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->string('sr_no');
            $table->string('merchant_id');
            $table->string('bank_id');
            $table->string('payment_id');
            $table->string('pgi_ref_no');
            $table->string('ref_1');
            $table->string('ref_2');
            $table->string('ref_3');
            $table->string('date_of_txn');
            $table->string('amount');
            $table->string('status');
            $table->string('ip_address');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airtel_money_record');
    }
}
