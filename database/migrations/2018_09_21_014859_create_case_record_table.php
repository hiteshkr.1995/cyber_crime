<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_record', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_id');
            $table->string('applicant_name');
            $table->bigInteger('applicant_mobile');
            $table->string('applicant_email');
            $table->string('applicant_address');
            $table->tinyInteger('case_status')->default(0);
            $table->string('case_type');
            $table->text('remarks')->nullable();
            $table->integer('priority')->nullable();
            $table->date('expected_to_close')->nullable();
            $table->integer('application_from')->nullable();
            $table->integer('case_tranfer_to')->nullable();
            $table->string('image')->nullable();
            $table->integer('assigned_to')->default(0);
            $table->json('history')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_record');
    }
}
