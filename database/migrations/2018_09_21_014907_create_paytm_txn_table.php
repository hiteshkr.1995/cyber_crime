<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaytmTxnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paytm_txn', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->bigInteger('custId');
            $table->double('txnAmt');
            $table->string('txnDate');
            $table->double('orderId');
            $table->string('bankGateway');
            $table->string('paymentMode');
            $table->string('bankTxnId');
            $table->string('binNumber');
            $table->integer('lastFour');
            $table->string('issuingBank');
            $table->string('remote_ip');
            $table->string('channel');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paytm_txn');
    }
}
