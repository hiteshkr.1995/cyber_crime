<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcavenueRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccavenue_record', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->string('auth_code');
            $table->string('ccavenue_reference_no');
            $table->string('order_card_type');
            $table->string('order_card_name');
            $table->string('order_bill_name');
            $table->string('order_bill_address');
            $table->string('order_bill_city');
            $table->string('order_bill_state');
            $table->string('order_bill_zip');
            $table->string('order_bill_country');
            $table->string('order_bill_telephone');
            $table->string('order_bill_email');
            $table->string('order_ip');
            $table->string('order_url');
            $table->string('order_no');
            $table->string('order_date_time');
            $table->string('order_amount');
            $table->string('order_currency');
            $table->string('order_status');
            $table->string('order_bank_qsi_no');
            $table->string('order_reciept_no');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccavenue_record');
    }
}
