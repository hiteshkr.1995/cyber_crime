<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCdrVodafoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cdr_vodafone', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->string('A_PARTY_NUMBER');
            $table->string('B_PARTY_NUMBER');
            $table->string('CALL_DATE');
            $table->string('CALL_TIME');
            $table->string('DURATION');
            $table->string('CELL_ID');
            $table->string('LAST_CELL_ID_A');
            $table->string('CALL_TYPE');
            $table->string('IMEI');
            $table->string('IMSI');
            $table->string('PP_PO');
            $table->string('SMS_CENTRE');
            $table->string('ROAMING_NW_CIED');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cdr_vodafone');
    }
}
