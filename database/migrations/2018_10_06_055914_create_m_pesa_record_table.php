<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMPesaRecordTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('m_pesa_record', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('case_record_id');
			$table->string('entity_circle');
			$table->string('entity_type');
			$table->string('entity_name');
			$table->string('entity_wallet_no');
			$table->string('entity_id');
			$table->string('transaction_id');
			$table->string('transaction_state');
			$table->string('from_account_number');
			$table->string('to_account_number');
			$table->string('amount');
			$table->string('transaction_start_time');
			$table->string('transaction_end_time');
			$table->string('service_name');
			$table->string('transaction_status');
			$table->string('transaction_failure_reason');
			$table->string('bank_name');
			$table->string('ifsc_code');
			$table->string('source_channel_type');
			$table->string('source_ip');
			$table->string('source_port');
			$table->string('destination_ip');
			$table->string('destination_port');
			$table->string('device_used');
			$table->string('device_make_model');
			$table->string('os_application_Version');
			$table->string('agent_msisdn');
			$table->string('assistant_msisdn');
			$table->string('telescopic_flag');
			$table->enum('is_malicious', [0, 1])->default(0);
			$table->bigInteger('uploaded_file');
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->timestamp('created_at')->useCurrent();
			$table->timestamp('updated_at')->useCurrent();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('m_pesa_record');
	}
}
