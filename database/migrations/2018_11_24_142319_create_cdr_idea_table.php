<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCdrIdeaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cdr_idea', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->string('Calling_A_Party');
            $table->string('LRN');
            $table->string('Called_B_Party');
            $table->string('Call_Date');
            $table->string('Call_Time');
            $table->string('Call_Duration_in_second');
            $table->string('First_Cell_ID');
            $table->string('Last_Cell_ID');
            $table->string('Call_Type');
            $table->string('IMEI');
            $table->string('IMSI');
            $table->string('Type_Of_Connection');
            $table->string('SMS_Center_Number');
            $table->string('First_Roaming_Network_Cell_Id_of_A');
            $table->string('Roam_Circle');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cdr_idea');
    }
}
