<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCdrTelenorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cdr_telenor', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->string('A_Party_Telephone_Number');
            $table->string('B_Party_Telephone_Number');
            $table->string('OriginalCallingNumber');
            $table->string('Call_Date');
            $table->string('Call_Time');
            $table->string('Call_Duration_In_seconds');
            $table->string('First_Cell_ID_of_Party_A');
            $table->string('Last_Cell_ID_Of_Party_A');
            $table->string('Call_Type_IN_OUT_SMS_IN_SMS_OUT');
            $table->string('IMEI_of_A');
            $table->string('IMSI_of_A');
            $table->string('Type_of_Connection_Pre_paid_Post_Paid )');
            $table->string('SMS_Center_Number');
            $table->string('First_Roaming_Network_Circl_ID_of_A');
            $table->string('Home_Circle');
            $table->string('Roaming_Circle_Operator');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cdr_telenor');
    }
}
