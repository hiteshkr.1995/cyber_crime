<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCdrJioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cdr_jio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->string('Calling_Party_Telephone_Number');
            $table->string('Called_Party_Telephone_Number');
            $table->string('Call_Forwarding');
            $table->string('LRN_Called_No');
            $table->string('Call_Date');
            $table->string('Call_Time');
            $table->string('Call_Termination_Time');
            $table->string('Call_Duration');
            $table->string('First_Cell_ID');
            $table->string('Last_Cell_ID');
            $table->string('Call_Type');
            $table->string('SMS_Center_Number');
            $table->string('IMEI');
            $table->string('IMSI');
            $table->string('Roaming_Circle_Name');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cdr_jio');
    }
}
