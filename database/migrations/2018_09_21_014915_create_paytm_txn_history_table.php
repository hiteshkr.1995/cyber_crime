<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaytmTxnHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paytm_txn_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->bigInteger('customer_id');
            $table->string('firstName');
            $table->string('lastName');
            $table->bigInteger('mobile');
            $table->string('isMerchant');
            $table->string('txn_created_at');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paytm_txn_history');
    }
}
