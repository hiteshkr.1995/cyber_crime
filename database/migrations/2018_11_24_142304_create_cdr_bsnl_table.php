<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCdrBsnlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cdr_bsnl', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->string('Calling_A_Party');
            $table->string('Call_Type');
            $table->string('Connection_Type');
            $table->string('Called_B_Party');
            $table->string('LRN');
            $table->string('Call_Date');
            $table->string('Call_Time');
            $table->string('Call_Duration');
            $table->string('First_Cell_Name');
            $table->string('First_CellID');
            $table->string('Last_Cell_Name');
            $table->string('Last_CellID');
            $table->string('SMS_Center');
            $table->string('Service_Type');
            $table->string('IMEI');
            $table->string('IMSI');
            $table->string('CALL_FWD_NO');
            $table->string('Roaming_Circle');
            $table->string('Switch_ID');
            $table->string('IN_TG');
            $table->string('OUT_TG');
            $table->string('LAT_LONG');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cdr_bsnl');
    }
}
