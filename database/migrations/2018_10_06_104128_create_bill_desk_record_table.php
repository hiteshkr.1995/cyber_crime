<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillDeskRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_desk_record', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->string('sr_no');
            $table->string('merchant_id');
            $table->string('transaction_id_on_our_platform');
            $table->string('dongle_number');
            $table->string('account_number');
            $table->string('transaction_date_and_time');
            $table->string('transaction_amount');
            $table->string('ip_address_from_where_transaction_initiated');
            $table->string('partial_card_number');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_desk_record');
    }
}
