<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaytmTxnPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paytm_txn_person', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_record_id');
            $table->bigInteger('payerId');
            $table->string('accountNumber');
            $table->string('ifscCode');
            $table->string('bankName');
            $table->double('txnamount');
            $table->string('txndate');
            $table->string('message');
            $table->string('strId');
            $table->string('bankTxnId');
            $table->double('fraud_amount_used');
            $table->string('deviceID');
            $table->string('ipAddress');
            $table->enum('is_malicious', [0, 1])->default(0);
            $table->bigInteger('uploaded_file');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paytm_txn_person');
    }
}
