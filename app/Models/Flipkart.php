<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Flipkart extends Model
{
	// Define table name
	protected $table = 'flipkart_record';

	protected $fillable = [
		'case_record_id',
		'transaction_time',
		'merchant_txn_id',
		'amount',
		'card_number',
		'card_bank',
		'first_name',
		'email_id',
		'phone_number',
		'user_ip_address',
		'shipping_address',
		'shipping_city',
		'shipping_state',
		'order_id',
		'order_status',
		'product',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Flipkart',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}
}
