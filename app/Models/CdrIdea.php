<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrIdea extends Model
{
	protected $table = 'cdr_idea';

	protected $fillable = [
		'case_record_id',
		'Calling_A_Party',
		'LRN',
		'Called_B_Party',
		'Call_Date',
		'Call_Time',
		'Call_Duration_in_second',
		'First_Cell_ID',
		'Last_Cell_ID',
		'Call_Type',
		'IMEI',
		'IMSI',
		'Type_Of_Connection',
		'SMS_Center_Number',
		'First_Roaming_Network_Cell_Id_of_A',
		'Roam_Circle',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Idea',
			'directory' => 'Cdr',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

	public function scopeSearchImei($query, $value)
	{
		return $query->where('IMEI', 'LIKE' ,"%{$value}%")->get();
	}

}
