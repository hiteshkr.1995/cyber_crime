<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrAirtel extends Model
{
	protected $table = 'cdr_airtel';

	protected $fillable = [
		'case_record_id',
		'calling_no',
		'called_no',
		'date',
		'time',
		'durs',
		'cell1',
		'cell2',
		'call_type',
		'imei',
		'imsino',
		'smsc',
		'roam_nw',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Airtel',
			'directory' => 'cdr',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

	public function scopeSearchImei($query, $value)
	{
		return $query->where('imei', 'LIKE' ,"%{$value}%")->get();
	}

}
