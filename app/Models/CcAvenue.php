<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CcAvenue extends Model
{
	// Define table name
	protected $table = 'ccavenue_record';

	protected $fillable = [
		'case_record_id',
		'auth_code',
		'ccavenue_reference_no',
		'order_card_type',
		'order_card_name',
		'order_bill_name',
		'order_bill_address',
		'order_bill_city',
		'order_bill_state',
		'order_bill_zip',
		'order_bill_country',
		'order_bill_telephone',
		'order_bill_email',
		'order_ip',
		'order_url',
		'order_no',
		'order_date_time',
		'order_amount',
		'order_currency',
		'order_status',
		'order_bank_qsi_no',
		'order_reciept_no',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'CCAvenue',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

}
