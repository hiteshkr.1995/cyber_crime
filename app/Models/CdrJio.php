<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrJio extends Model
{
	protected $table = 'cdr_jio';

	protected $fillable = [
		'case_record_id',
		'Calling_Party_Telephone_Number',
		'Called_Party_Telephone_Number',
		'Call_Forwarding',
		'LRN_Called_No',
		'Call_Date',
		'Call_Time',
		'Call_Termination_Time',
		'Call_Duration',
		'First_Cell_ID',
		'Last_Cell_ID',
		'Call_Type',
		'SMS_Center_Number',
		'IMEI',
		'IMSI',
		'Roaming_Circle_Name',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Jio',
			'directory' => 'Cdr',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

	public function scopeSearchImei($query, $value)
	{
		return $query->where('IMEI', 'LIKE' ,"%{$value}%")->get();
	}

}
