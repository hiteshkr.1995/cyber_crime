<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrBSNL extends Model
{
	protected $table = 'cdr_bsnl';

	protected $fillable = [
		'case_record_id',
		'Calling_A_Party',
		'Call_Type',
		'Connection_Type',
		'Called_B_Party',
		'LRN',
		'Call_Date',
		'Call_Time',
		'Call_Duration',
		'First_Cell_Name',
		'First_CellID',
		'Last_Cell_Name',
		'Last_CellID',
		'SMS_Center',
		'Service_Type',
		'IMEI',
		'IMSI',
		'CALL_FWD_NO',
		'Roaming_Circle',
		'Switch_ID',
		'IN_TG',
		'OUT_TG',
		'LAT_LONG',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Bsnl',
			'directory' => 'cdr',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

	public function scopeSearchImei($query, $value)
	{
		return $query->where('IMEI', 'LIKE' ,"%{$value}%")->get();
	}

}
