<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CaseType extends Model
{
	protected $table = 'case_type';

	protected $fillable = [
		'name',
	];
}
