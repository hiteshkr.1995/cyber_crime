<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AirtelMoney extends Model
{
	// Define table name
	protected $table = 'airtel_money_record';

	protected $fillable = [
		'case_record_id',
		'sr_no',
		'merchant_id',
		'bank_id',
		'payment_id',
		'pgi_ref_no',
		'ref_1',
		'ref_2',
		'ref_3',
		'date_of_txn',
		'amount',
		'status',
		'ip_address',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by'
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Airtel Money',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

}