<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaytmTxnHistory extends Model
{

	// Define table name
	protected $table = 'paytm_txn_history';

	protected $fillable = [
		'case_record_id',
		'customer_id',
		'firstName',
		'lastName',
		'mobile',
		'isMerchant',
		'txn_created_at',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

}
