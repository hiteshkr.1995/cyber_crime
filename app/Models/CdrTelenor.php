<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrTelenor extends Model
{
	protected $table = 'cdr_telenor';

	protected $fillable = [
		'case_record_id',
		'A_Party_Telephone_Number',
		'B_Party_Telephone_Number',
		'OriginalCallingNumber',
		'Call_Date',
		'Call_Time',
		'Call_Duration_In_seconds',
		'First_Cell_ID_of_Party_A',
		'Last_Cell_ID_Of_Party_A',
		'Call_Type_IN_OUT_SMS_IN_SMS_OUT',
		'IMEI_of_A',
		'IMSI_of_A',
		'Type_of_Connection_Pre_paid_Post_Paid )',
		'SMS_Center_Number',
		'First_Roaming_Network_Circl_ID_of_A',
		'Home_Circle',
		'Roaming_Circle_Operator',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Telenor',
			'directory' => 'cdr',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

	public function scopeSearchImei($query, $value)
	{
		return $query->where('IMEI_of_A', 'LIKE' ,"%{$value}%")->get();
	}

}
