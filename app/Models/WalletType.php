<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletType extends Model
{
	protected $table = 'wallet_type';

	protected $fillable = [
		'name','model_name'
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Wallet',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}
}
