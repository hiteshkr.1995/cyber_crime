<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaytmTxn extends Model
{
	// Define table name
	protected $table = 'paytm_txn';

	protected $fillable = [
		'case_record_id',
		'custId',
		'txnAmt',
		'txnDate',
		'orderId',
		'bankGateway',
		'paymentMode',
		'bankTxnId',
		'binNumber',
		'lastFour',
		'issuingBank',
		'remote_ip',
		'channel',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Paytm Transaction',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

}
