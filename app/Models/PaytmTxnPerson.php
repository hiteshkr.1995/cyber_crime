<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaytmTxnPerson extends Model
{

	// Define table name
	protected $table = 'paytm_txn_person';

	protected $fillable = [
		'case_record_id',
		'payerId',
		'accountNumber',
		'ifscCode',
		'bankName',
		'txnamount',
		'txndate',
		'message',
		'strId',
		'bankTxnId',
		'fraud_amount_used',
		'deviceID',
		'ipAddress',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Paytm Transaction Person',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

}
