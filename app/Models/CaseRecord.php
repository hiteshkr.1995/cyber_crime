<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseRecord extends Model
{
	use SoftDeletes;

	protected $table = 'case_record';
	protected $fillable = [
		'case_id',
		'applicant_name',
		'applicant_mobile',
		'applicant_email',
		'applicant_address',
		'case_status',
		'case_type',
		'remarks',
		'priority',
		'expected_to_close',
		'application_from',
		'case_tranfer_to',
		'image',
		'assigned_to',
		'history',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Case Record',
			'directory' => 'case record',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

	public function caseType()
	{
		return $this->belongsTo('App\Models\CaseType', 'case_type');
	}

	public function assignedTo()
	{
		return $this->belongsTo('App\Models\User', 'assigned_to');
	}

	public function createdBy()
	{
		return $this->belongsTo('App\Models\User', 'created_by');
	}

	public function updatedBy()
	{
		return $this->belongsTo('App\Models\User', 'updated_by');
	}
}
