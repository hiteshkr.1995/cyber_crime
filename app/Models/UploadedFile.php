<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UploadedFile extends Model
{
	use SoftDeletes;

	// Define table name
	protected $table = 'uploaded_file';

	protected $fillable = ['case_record_id', 'option', 'for', 'path', 'name', 'original_name', 'sha1', 'created_by', 'updated_by'];

	public function scopeExists($query, $caseId, $sha1)
	{
		return $query->where('case_record_id', $caseId)->where('sha1', $sha1);
	}
}
