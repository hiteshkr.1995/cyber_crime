<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use App\Models\CaseRecord;

class User extends Authenticatable
{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'username','password','email','name','mobile_number','designation','privilege','status','created_by','updated_by',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token', 'email_verified_at',
	];

	/**
	 * Send the password reset notification.
	 *
	 * @param  string  $token
	 * @return void
	 */
	public function sendPasswordResetNotification($token)
	{
		$this->notify(new ResetPasswordNotification($token));
	}

	public function isDcp()
	{
		if (auth()->user()->designation == 2) {

			return TRUE;

		} else {

			return FALSE;

		}
	}

	public function privilege()
	{
		if (auth()->user()->privilege == 1) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}


class ResetPasswordNotification extends ResetPassword
{
	public function toMail($notifiable)
	{
		if (static::$toMailCallback) {
			return call_user_func(static::$toMailCallback, $notifiable, $this->token);
		}
		return (new MailMessage)
		->from('info@codelancers.in', 'Hello Check')
		->subject(Lang::getFromJson('Reset Password Notification Update'))
		->line(Lang::getFromJson('You are receiving this email because we received a password reset request for your account.'))
		->action(Lang::getFromJson('Reset Password'), url(route('password.reset', $this->token, false)))
		->line(Lang::getFromJson('If you did not request a password reset, no further action is required.'));
	}
}