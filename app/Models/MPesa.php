<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MPesa extends Model
{
	// Define table name
	protected $table = 'm_pesa_record';

	protected $fillable = [
		'case_record_id',
		'entity_circle',
		'entity_type',
		'entity_name',
		'entity_wallet_no',
		'entity_id',
		'transaction_id',
		'transaction_state',
		'from_account_number',
		'to_account_number',
		'amount',
		'transaction_start_time',
		'transaction_end_time',
		'service_name',
		'transaction_status',
		'transaction_failure_reason',
		'bank_name',
		'ifsc_code',
		'source_channel_type',
		'source_ip',
		'source_port',
		'destination_ip',
		'destination_port',
		'device_used',
		'device_make_model',
		'os_application_Version',
		'agent_msisdn',
		'assistant_msisdn',
		'telescopic_flag',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Mpesa',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}
}
