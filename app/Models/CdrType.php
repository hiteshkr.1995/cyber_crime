<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrType extends Model
{
	protected $table = 'cdr_type';

	protected $fillable = [
		'name','model_name'
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Cdr',
			'directory' => 'cdr',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}
}
