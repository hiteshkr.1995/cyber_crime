<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillDesk extends Model
{
	// Define table name
	protected $table = 'bill_desk_record';

	protected $fillable = [
		'case_record_id',
		'sr_no',
		'merchant_id',
		'transaction_id_on_our_platform',
		'dongle_number',
		'account_number',
		'transaction_date_and_time',
		'transaction_amount',
		'ip_address_from_where_transaction_initiated',
		'partial_card_number',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Bill Desk',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}
}
