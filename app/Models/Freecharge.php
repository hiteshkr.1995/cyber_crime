<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Freecharge extends Model
{
	// Define table name
	protected $table = 'freecharge_record';

	protected $fillable = [
		'case_record_id',
		'order_id',
		'pg_id',
		'amount',
		'card_no',
		'ip_address',
		'email_id',
		'transaction_date_time',
		'registered_mob_no',
		'status',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Free Recharge',
			'directory' => 'wallet',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}
}
