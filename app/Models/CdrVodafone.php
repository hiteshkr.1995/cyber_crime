<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrVodafone extends Model
{
	protected $table = 'cdr_vodafone';

	protected $fillable = [
		'case_record_id',
		'A_PARTY_NUMBER',
		'B_PARTY_NUMBER',
		'CALL_DATE',
		'CALL_TIME',
		'DURATION',
		'CELL_ID',
		'LAST_CELL_ID_A',
		'CALL_TYPE',
		'IMEI',
		'IMSI',
		'PP_PO',
		'SMS_CENTRE',
		'ROAMING_NW_CIED',
		'is_malicious',
		'uploaded_file',
		'created_by',
		'updated_by',
	];

	static function essence($key = NULL)
	{

		$data = array(
			'name' => 'Vodafone',
			'directory' => 'cdr',
		);

		return array_key_exists($key, $data) ? $data[$key] : NULL;

	}

	public function scopeSearchImei($query, $value)
	{
		return $query->where('IMEI', 'LIKE' ,"%{$value}%")->get();
	}

}
