<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserRegister;
use App\Models\Designation;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            if (auth()->user()->isDcp()) {
                return $next($request);
            } else {
                return abort(404);
            }
        });

    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    public function showRegistrationForm()
    {
        $designations = Designation::all();
        return view('auth.register')->with([
            'designations' => $designations,
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email',
            'mobileNumber' => 'required|numeric|digits:10',
            'designation' => 'required|numeric|exists:designation,id',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        //define privilege
        switch ($data['designation']) {
            case 'DCP':
                $privilege = 1;
                break;

            case 'ACP':
                $privilege = 2;
                break;

            case 'PSI':
                $privilege = 3;
                break;

            case 'HC':
                $privilege = 4;
                break;

            default:
                $privilege = 0;
                break;
        }

        /*
        $user_id : get last id from the database and do increment
        $username :  generate the username
        */
        $lastuser = User::select('id')->orderBy('id', 'DESC')->first();
        !empty($lastuser)?$lastuser_id=$lastuser->id:$lastuser_id=0;
        $username = designation_name($data['designation']).'_'.($lastuser_id+1);

        /* generate a random password of 8 DIGITS */
        $password = mt_rand(10000000,99999999);

        // Crete user
        $user = User::create([
            'username' => $username,
            'password' => Hash::make($password),
            'email' => $data['email'],
            'name' => $data['name'],
            'mobile_number' => $data['mobileNumber'],
            'designation' => $data['designation'],
            'privilege' => $privilege,
            'status' => 0,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
        echo '<pre>'.$user.'</pre>';
        // Sent Mail For Welcome
        $mailData = array(
            'email' => $data['email'],
            'password' => $password,
        );

       Mail::to($data['email'])->send(new NewUserRegister($mailData));

        return $user;
    }
}