<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Exception;
use App\Models\PaytmTxn;
use App\Models\PaytmTxnPerson;
use App\Models\PaytmTxnHistory;
use App\Models\CdrType;
use App\Models\AirtelMoney;
use App\Models\BillDesk;
use App\Models\CcAvenue;
use App\Models\Flipkart;
use App\Models\Freecharge;
use App\Models\MPesa;

use App\Models\CdrAirtel;
use App\Models\CdrBSNL;
use App\Models\CdrIdea;
use App\Models\CdrJio;
use App\Models\CdrTelenor;
use App\Models\CdrVodafone;

use App\Models\UploadedFile;
use App\Models\CaseRecord;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Validator;
use App\Classes\CaseRecordClass;

class CdrController extends Controller
{

	private $uploadedFile;
	private $fileValue;

	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('privilege')->only('create', 'store', 'assign', 'destroy');
		// $this->middleware('CheckCaseRecordAssign')->only('isMalicious');

		$this->uploadedFile = array();
		$this->fileValue = array();
	}

	/**
	 * Display a form for upload the excel file.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function addData(Request $request, $caseEncryptId)
	{
		$cdrType = CdrType::all();
		return view('panel.cdr.add-data', compact('cdrType', 'caseEncryptId'));
	}

	// public function viewData(Request $request,$caseEncryptId)
	// {
	// 	$caseId = param_decrypt($caseEncryptId);

	// 	$caseRecord = CaseRecord::findOrFail($caseId);

	// 	$cdrType = CdrType::all();

	// 	if (!empty($request->cdr)) {

	// 		$cdrId = $request->cdr;

	// 		$selectedCdrType = CdrType::findOrFail($cdrId);

	// 		$cdrName = $selectedCdrType->name;
	// 		$modelName = explode(",",$selectedCdrType->model_name);

	// 		$dataArray = array();

	// 		$fields = ['case_record_id', 'uploaded_file', 'created_by', 'updated_by', 'created_at', 'updated_at'];

	// 		foreach ($modelName as $key => $value) {
	// 			$Table = constant('MODEL_PATH').$value;
	// 			$result = $Table::where('case_record_id', $caseId)->get();
	// 			$result = $result->makeHidden($fields)->toArray();
	// 			array_push($dataArray, $result);
	// 		}

	// 		if (count($dataArray[0]) > 0) {
	// 			$modelName = $selectedCdrType->model_name;
	// 			return view('panel.cdr.view-data', compact('dataArray', 'caseEncryptId', 'cdrType', 'modelName'));
	// 		} else {
	// 			return redirect()->route('cdr.data.view', $caseEncryptId)->with('warning', $cdrName.'  no record found!');
	// 		}

	// 	} else {

	// 		return view('panel.cdr.view-data', compact('cdrType', 'caseEncryptId'));

	// 	}
	// }

	public function uploadFile(Request $request, $caseEncryptId)
	{
		$request->validate([
			'cdr' => 'required|exists:cdr_type,id',
			'file' => 'required|file',
		]);
		try {
			DB::beginTransaction();
			$caseId = param_decrypt($caseEncryptId);

			$response = redirect()->route('cdr.data.add', $caseEncryptId);

			$this->fileValue = file_nest($_FILES['file']);

			$file_get_contents = file_get_contents($this->fileValue->tmp_name);

			$this->csvFileUpload($caseId, $file_get_contents);
			upload_fileAs($this->uploadedFile->path, $request->file, $this->uploadedFile->name);

			DB::commit();
			return $response->with('success', 'Successfully File Uploaded!');
		} catch (Exception $e) {

			DB::rollBack();
			return $response->with('error', $e->getMessage());

		}
	}

	public function submitData(Request $request,$caseEncryptId)
	{
		$request->validate([
			'cdr' => 'required|exists:cdr_type,id',
			'file' => 'required|mimes:csv,txt',
		]);

		$response = redirect()->route('cdr.data.add', $caseEncryptId);

		try {
			DB::beginTransaction();

			$caseId = param_decrypt($caseEncryptId);

			$cdr = $request->cdr;

			$file = $request->file;
			$this->fileValue = file_nest($_FILES['file']);

			switch ($cdr) {
				case 1:
					$result = $this->airtelAddData($file, $caseId);
				break;
				case 2:
					$result = $this->bsnlAddData($file, $caseId);
				break;
				case 3:
					$result = $this->ideaAddData($file, $caseId);
				break;
				case 4:
					$result = $this->jioAddData($file, $caseId);
				break;
				case 5:
					$result = $this->telenorAddData($file, $caseId);
				break;
				case 6:
					$result = $this->vodafoneAddData($file, $caseId);
				break;
				default:
					DB::commit();
					return $response->with('error', 'Please select one of the wallet!');
			}

			if ($result === TRUE) {

				upload_fileAs('cdr', $file, $this->uploadedFile->name);

				DB::commit();
				return $response->with('success', 'Successfully File Uploaded!');

			} else {

				throw new Exception("Sorry something went wrong!");

			}
		} catch (Exception $e) {

			DB::rollBack();
			return $response->with('error', $e->getMessage());

		}
	}

	public function airtelAddData($file, $caseId)
	{
		$header = [
			'Calling No',
			'Called No',
			'Date',
			'Time',
			'Dur(s)',
			'Cell1',
			'Cell2',
			'Call Type',
			'IMEI',
			'IMSI No',
			'SMSC',
			'Roam Nw',
		];

		$tableFields = [
			'calling_no',
			'called_no',
			'date','time',
			'durs',
			'cell1',
			'cell2',
			'call_type',
			'imei',
			'imsino',
			'smsc',
			'roam_nw',
		];

		$csvData = csv_reader($file, $header, $tableFields);

		$this->csvFileUpload($caseId, $csvData);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($csvData as $key => $value) {

			$csvData[$key] = array_merge($value,$add_column);

		}

		// echo "<pre>";
		// print_r($csvData);exit();
		CdrAirtel::insert($csvData);

		return TRUE;

	}

	public function bsnlAddData($file, $caseId)
	{
		$header = [
			'Calling (A) Party',
			'Call Type',
			'Connection Type',
			'Called (B) Party',
			'LRN',
			'Call Date',
			'Call Time',
			'Call Duration',
			'First Cell Name',
			'First CellID',
			'Last Cell Name',
			'Last CellID',
			'SMS Center',
			'Service Type',
			'IMEI',
			'IMSI',
			'CALL_FWD_NO',
			'Roaming Circle',
			'Switch ID',
			'IN_TG',
			'OUT_TG',
			'LAT/LONG',
		];

		$tableFields = [
			'Calling_A_Party',
			'Call_Type',
			'Connection_Type',
			'Called_B_Party',
			'LRN',
			'Call_Date',
			'Call_Time',
			'Call_Duration',
			'First_Cell_Name',
			'First_CellID',
			'Last_Cell_Name',
			'Last_CellID',
			'SMS_Center',
			'Service_Type',
			'IMEI',
			'IMSI',
			'CALL_FWD_NO',
			'Roaming_Circle',
			'Switch_ID',
			'IN_TG',
			'OUT_TG',
			'LAT_LONG',
		];

		$csvData = csv_reader($file, $header, $tableFields);

		$this->csvFileUpload($caseId, $csvData);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($csvData as $key => $value) {

			$csvData[$key] = array_merge($value,$add_column);

		}

		$CdrBSNL = new CdrBSNL;

		import_data($CdrBSNL, $csvData);

		return TRUE;

	}

	public function ideaAddData($file, $caseId)
	{
		$header = [
			'S. No.',
			'Calling (A) Party',
			'LRN',
			'Called (B) Party',
			'Call Date',
			'Call Time',
			'Call Duration (in second)',
			'First Cell ID',
			'Last Cell ID',
			'Call Type',
			'IMEI',
			'IMSI',
			'Type Of Connection',
			'SMS Center Number',
			'First Roaming Network Cell Id of A',
			'Roam Circle',
		];

		$tableFields = [
			'S_No',
			'Calling_A_Party',
			'LRN',
			'Called_B_Party',
			'Call_Date',
			'Call_Time',
			'Call_Duration_in_second',
			'First_Cell_ID',
			'Last_Cell_ID',
			'Call_Type',
			'IMEI',
			'IMSI',
			'Type_Of_Connection',
			'SMS_Center_Number',
			'First_Roaming_Network_Cell_Id_of_A',
			'Roam_Circle',
		];

		$csvData = csv_reader($file, $header, $tableFields);

		$this->csvFileUpload($caseId, $csvData);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($csvData as $key => $value) {
			array_shift($value);
			$csvData[$key] = array_merge($value,$add_column);
		}

		// echo "<pre>";
		// print_r($csvData);exit();
		CdrIdea::insert($csvData);

		return TRUE;

	}

	public function jioAddData($file, $caseId)
	{
		$header = [
			'Calling Party Telephone Number',
			'Called Party Telephone Number',
			'Call Forwarding',
			'LRN Called No',
			'Call Date',
			'Call Time',
			'Call Termination Time',
			'Call Duration',
			'First Cell ID',
			'Last Cell ID',
			'Call Type',
			'SMS Center Number',
			'IMEI',
			'IMSI',
			'Roaming Circle Name',
		];

		$tableFields = [
			'Calling_Party_Telephone_Number',
			'Called_Party_Telephone_Number',
			'Call_Forwarding',
			'LRN_Called_No',
			'Call_Date',
			'Call_Time',
			'Call_Termination_Time',
			'Call_Duration',
			'First_Cell_ID',
			'Last_Cell_ID',
			'Call_Type',
			'SMS_Center_Number',
			'IMEI',
			'IMSI',
			'Roaming_Circle_Name',
		];

		$csvData = csv_reader($file, $header, $tableFields);

		$this->csvFileUpload($caseId, $csvData);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($csvData as $key => $value) {

			$csvData[$key] = array_merge($value,$add_column);

		}

		// echo "<pre>";
		// print_r($csvData);exit();
		CdrJio::insert($csvData);

		return TRUE;

	}

	public function telenorAddData($file, $caseId)
	{
		$header = [
			'(A) Party Telephone Number',
			'(B) Party Telephone Number',
			'OriginalCallingNumber',
			'Call Date',
			'Call Time',
			'Call Duration(In seconds)',
			'First Cell ID of Party(A)',
			'Last Cell ID Of Party(A)',
			'Call Type (IN/OUT/SMS IN/SMS OUT',
			'IMEI of A',
			'IMSI of A',
			'Type of Connection (Pre-paid / Post - Paid )',
			'SMS Center Number',
			'First Roaming Network Circle ID of A',
			'Home Circle',
			'Roaming Circle Operator',
		];

		$tableFields = [
			'A_Party_Telephone_Number',
			'B_Party_Telephone_Number',
			'OriginalCallingNumber',
			'Call_Date',
			'Call_Time',
			'Call_Duration_In_seconds',
			'First_Cell_ID_of_Party_A',
			'Last_Cell_ID_Of_Party_A',
			'Call_Type_IN_OUT_SMS_IN_SMS_OUT',
			'IMEI_of_A',
			'IMSI_of_A',
			'Type_of_Connection_Pre_paid_Post_Paid )',
			'SMS_Center_Number',
			'First_Roaming_Network_Circl_ID_of_A',
			'Home_Circle',
			'Roaming_Circle_Operator',
		];

		$csvData = csv_reader($file, $header, $tableFields);

		$this->csvFileUpload($caseId, $csvData);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($csvData as $key => $value) {

			$csvData[$key] = array_merge($value,$add_column);

		}

		// echo "<pre>";
		// print_r($csvData);exit();
		CdrTelenor::insert($csvData);

		return TRUE;

	}

	public function vodafoneAddData($file, $caseId)
	{
		$header = [
			'A PARTY NUMBER',
			'B PARTY NUMBER',
			'CALL_DATE',
			'CALL_TIME',
			'DURATION',
			'CELL_ID',
			'LAST_CELL_ID_A',
			'CALL_TYPE',
			'IMEI',
			'IMSI',
			'PP_PO',
			'SMS_CENTRE',
			'ROAMING_NW_CIED',
		];

		$tableFields = [
			'A_PARTY_NUMBER',
			'B_PARTY_NUMBER',
			'CALL_DATE',
			'CALL_TIME',
			'DURATION',
			'CELL_ID',
			'LAST_CELL_ID_A',
			'CALL_TYPE',
			'IMEI',
			'IMSI',
			'PP_PO',
			'SMS_CENTRE',
			'ROAMING_NW_CIED',
		];

		$csvData = csv_reader($file, $header, $tableFields);

		$this->csvFileUpload($caseId, $csvData);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($csvData as $key => $value) {

			$csvData[$key] = array_merge($value,$add_column);

		}

		// echo "<pre>";
		// print_r($csvData);exit();
		CdrVodafone::insert($csvData);

		return TRUE;

	}

	public function csvFileUpload($caseId, $csvData)
	{

		$sha1 = sha1($csvData);

		if (UploadedFile_exists($caseId, $sha1)) {

			throw new Exception("File already exists!");

		}

		$path = 'caserecord/'.$caseId.'/cdr/';

		$data = [
			'case_record_id' => $caseId,
			'option' => 'option2',
			'for' => $_REQUEST['cdr'],
			'name' => $this->fileValue->name_generated,
			'path' => $path,
			'original_name' => $this->fileValue->name,
			'sha1' => $sha1,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
		];

		return $this->uploadedFile = UploadedFile::create($data);

	}

	public function viewData(Request $request, $caseEncryptId)
	{
		$caseId = param_decrypt($caseEncryptId);

		$caseRecord = CaseRecord::findOrFail($caseId);

		$cdrType = CdrType::all();

		if (!empty($request->cdr)) {

		// print_r('cc');exit();
			$cdrId = $request->cdr;

			$dataArray = array();

			$selectedCdrType = CdrType::findOrFail($cdrId);

			$walletName = $selectedCdrType->name;
			$modelName = explode(",",$selectedCdrType->model_name);

			foreach ($modelName as $key => $value) {
				array_push($dataArray, columns_name($value));
			}

			if (count($dataArray) > 0) {
				$modelName = $selectedCdrType->model_name;
				return view('panel.cdr.view-data', compact('dataArray', 'caseEncryptId', 'cdrType', 'modelName'));
			} else {
				return redirect()->route('cdr.data.view', $caseEncryptId)->with('warning', $walletName.'  no record found!');
			}

		} else {

			return view('panel.cdr.view-data', compact('cdrType', 'caseEncryptId'));

		}
	}

}
