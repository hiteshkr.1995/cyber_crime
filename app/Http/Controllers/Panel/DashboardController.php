<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Exception;
use App\Models\CaseRecord;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$totalCase = CaseRecord::count();
		$completedCase = CaseRecord::where('case_status', 0)->count();
		$pendingCase = $totalCase - $completedCase;

		return view('panel.index', compact('totalCase', 'completedCase', 'pendingCase'));
	}
}