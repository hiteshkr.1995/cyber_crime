<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Exception;
use App\Models\User;
use App\Models\Designation;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware(function ($request, $next) {
			if (auth()->user()->isDcp()) {
				return $next($request);
			} else {
				return abort(404);
			}
		});
		// $this->middleware('privilege')->only('create', 'store', 'assign', 'destroy');
		// $this->middleware('CheckCaseRecordAssign')->only('show', 'edit', 'update', 'pdfCaseRecord');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$user = User::all();
		return view('panel.user.index', compact('user'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($userEncryptId)
	{
		$userId = param_decrypt($userEncryptId);

		$user = User::findOrFail($userId);

		return view('panel.user.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($userEncryptId)
	{
		$userId = param_decrypt($userEncryptId);

		$user = User::findOrFail($userId);
		$designations = Designation::all();

		return view('panel.user.edit', compact('user', 'designations'));		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $userEncryptId)
	{

		$userId = param_decrypt($userEncryptId);
		// return $request->all();
		// Validate request
		if (Auth::user()->id == $userId) {
			$request->validate([
				'name' => 'required|string|max:255',
				'email' => 'required|string|email|max:255|unique:users,email,'.$userId,
				'mobileNumber' => 'required|numeric|digits:10',
			]);
		} else {
			$request->validate([
				'name' => 'required|string|max:255',
				'email' => 'required|string|email|max:255|unique:users,email,'.$userId,
				'mobileNumber' => 'required|numeric|digits:10',
				'designation' => 'required|numeric|exists:designation,id',
			]);
		}

		// Store request in valiable
		$name = $request->name;
		$mobileNumber = $request->mobileNumber;
		$email = $request->email;
		$designation = $request->designation;
		$status = $request->status;

		// Update caseRecord
		if (Auth::user()->id == $userId) {
			$data = [
				'name' => $name,
				'mobile_number' => $mobileNumber,
				'applicant_email' => $email,
				'updated_by' => Auth::user()->id,
			];
		} else {
			$data = [
				'name' => $name,
				'mobile_number' => $mobileNumber,
				'applicant_email' => $email,
				'designation' => $designation,
				'status' => $status,
				'updated_by' => Auth::user()->id,
			];
		}

		User::find($userId)->update($data);

		return redirect()->route('user.show', $userEncryptId)->with('success', 'Successfully update user!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($userEncryptId)
	{
		$userId = param_decrypt($userEncryptId);

		if ($userId == Auth::user()->id) {
		} else {
			User::destroy(param_decrypt($userEncryptId));
		}
		return redirect()->route('user.index');
	}
}
