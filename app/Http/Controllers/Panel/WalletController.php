<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Exception;
use App\Models\PaytmTxn;
use App\Models\PaytmTxnPerson;
use App\Models\PaytmTxnHistory;
use App\Models\WalletType;
use App\Models\AirtelMoney;
use App\Models\BillDesk;
use App\Models\CcAvenue;
use App\Models\Flipkart;
use App\Models\Freecharge;
use App\Models\MPesa;
use App\Models\UploadedFile;
use App\Models\CaseRecord;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Validator;

class WalletController extends Controller
{

	private $uploadedFile;
	private $fileValue;

	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('privilege')->only('create', 'store', 'assign', 'destroy');

		$this->uploadedFile = array();
		$this->fileValue = array();
	}

	/**
	 * Display a form for upload the excel file.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function addData(Request $request, $caseEncryptId)
	{
		$walletType = WalletType::all();
		return view('panel.wallet.add-data', compact('walletType', 'caseEncryptId'));
	}

	public function uploadFile(Request $request, $caseEncryptId)
	{
		$request->validate([
			'wallet' => 'required|exists:wallet_type,id',
			'file' => 'required|file',
		]);
		try {
			DB::beginTransaction();
			$caseId = param_decrypt($caseEncryptId);

			$response = redirect()->route('wallet.data.add', $caseEncryptId);

			$this->fileValue = file_nest($_FILES['file']);

			$file_get_contents = file_get_contents($this->fileValue->tmp_name);

			$this->xlsxFileUpload($caseId, $file_get_contents);
			upload_fileAs($this->uploadedFile->path, $request->file, $this->uploadedFile->name);

			DB::commit();
			return $response->with('success', 'Successfully File Uploaded!');
		} catch (Exception $e) {

			DB::rollBack();
			return $response->with('error', $e->getMessage());

		}
	}

	public function submitData(Request $request, $caseEncryptId)
	{
		$request->validate([
			'wallet' => 'required|exists:wallet_type,id',
			'file' => 'required|mimes:xlsx,xls',
		]);

		$response = redirect()->route('wallet.data.add', $caseEncryptId);

		try {

			DB::beginTransaction();

			$caseId = param_decrypt($caseEncryptId);

			$wallet = $request->wallet;

			$file = $request->file;
			$this->fileValue = file_nest($_FILES['file']);

			switch ($wallet) {
				case 1:
					$result = $this->paytmAddData($file, $caseId);
				break;
				case 2:
					$result = $this->airtelMoneyAddData($file, $caseId);
				break;
				case 3:
					$result = $this->billDeskAddData($file, $caseId);
				break;
				case 4:
					$result = $this->ccAvenueAddData($file, $caseId);
				break;
				case 5:
					$result = $this->flipkartAddData($file, $caseId);
				break;
				case 6:
					$result = $this->freechargeAddData($file, $caseId);
				break;
				case 7:
					$result = $this->mPesaAddData($file, $caseId);
				break;
				default:
					DB::commit();
					return $response->with('error', 'Please select one of the wallet!');
			}

			if ($result === TRUE) {

				upload_fileAs('xlsx', $file, $this->uploadedFile->name);

				DB::commit();
				return $response->with('success', 'Successfully File Uploaded!');

			} else {

				throw new Exception("Sorry something went wrong!");

			}
		} catch (Exception $e) {

			DB::rollBack();
			return $response->with('error', $e->getMessage());

		}

	}

	public function paytmAddData($file, $caseId)
	{
		$rows = excel_reader($file);

		$headers = array('Transaction Details for Source Amount','Details of all customers involved in transaction via P2P',' Details of Amount transferred to BANK','Benificiary Details of Amount Consumed :','Refund Status','Details of IMPS done through Bank Account');

		$table1_headers = array("custId","txnAmt","txnDate","orderId","bankGateway","paymentMode","bankTxnId","binNumber","lastFour","issuingBank","remote_ip","channel");

		$table2_headers = array("customer_id","firstName","lastName","mobile","isMerchant","txn_created_at");

		$table3_headers = array("payerId","accountNumber","ifscCode","bankName","txnamount","txndate","message","strId","bankTxnId","fraud_amount_used","deviceID","ipAddress");

		$table4_headers = array("Benificiary IFSC","Benificiary Account Number","RRN");
		
		$i=0;
		$array = array();
		$header= '';
		$data = '';

		try {

			foreach ($rows as $row) {
				if(in_array($row[0],$headers))
				{
					next($row);	
					$header = TRUE;
				}	 
				else
				{
					if($header)
					{
						next($row);
						$data = TRUE;
						$header = FALSE;
						$i++;
					} elseif($data) {
						if(array_filter($row)) {
							if($i == 1) {
								$row = clean_data(array_slice($row,0,12));
								$array['paytm_txn'][] = array_combine($table1_headers , $row);
							}

							if($i == 2) {
								$row = clean_data(array_slice($row,0,6));
								$array['paytm_txn_history'][]=array_combine($table2_headers , $row);
							}
			
							if($i == 3) {
								$row = clean_data(array_slice($row,0,12));
								$array['paytm_txn_person'][]= array_combine($table3_headers,$row);
							}

							if($i == 4) { 
								$row = clean_data(array_slice($row,0,3));
								$array['paytm_imps'][]= array_combine($table4_headers,$row);
							}
						}
					} else {
						throw new Exception();
					}
				}
			}

		} catch (Exception $e) {

			throw new Exception("Please select proper wallet!");

		}

		$this->xlsxFileUpload($caseId, $array);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($array['paytm_txn'] as $key => $value) {
			$array['paytm_txn'][$key] = array_merge($value,$add_column);
		}

		foreach ($array['paytm_txn_history'] as $key => $value) {
			$array['paytm_txn_history'][$key] = array_merge($value,$add_column);
		}

		foreach ($array['paytm_txn_person'] as $key => $value) {
			$array['paytm_txn_person'][$key] = array_merge($value,$add_column);
		}

		foreach ($array['paytm_imps'] as $key => $value) {
			$array['paytm_imps'][$key] = array_merge($value,$add_column);
		}

		PaytmTxn::insert($array['paytm_txn']);
		PaytmTxnPerson::insert($array['paytm_txn_person']);
		PaytmTxnHistory::insert($array['paytm_txn_history']);

		return TRUE;

	}


	function airtelMoneyAddData($file, $caseId)
	{
		$headers = array('sr_no','merchant_id','bank_id','payment_id','pgi_ref_no','ref_1','ref_2','ref_3','date_of_txn','amount','status','ip_address');

		$rows = excel_reader($file);
		$array = array();

		$header = TRUE;

		try {

			foreach ($rows as $row) {
				if($header) {
					next($row);
					$header = FALSE;
				} else {
					$row = clean_data($row);
					$array['airtel_money_record'][] = array_combine($headers , $row);
				}
			}

		} catch (Exception $e) {

			throw new Exception("Please select proper wallet!");

		}

		$this->xlsxFileUpload($caseId, $array);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($array['airtel_money_record'] as $key => $value) {
			$array['airtel_money_record'][$key] = array_merge($value,$add_column);
		}
		
		AirtelMoney::insert($array['airtel_money_record']);

		return TRUE;
	}

	function billDeskAddData($file, $caseId)
	{
		$headers = array('sr_no','merchant_id','transaction_id_on_our_platform','dongle_number','account_number','transaction_date_and_time','transaction_amount','ip_address_from_where_transaction_initiated','partial_card_number');

		$rows = excel_reader($file);
		$array = array();

		$header = TRUE;

		try {

			foreach ($rows as $row) {
				if($header) {
					next($row);
					$header = FALSE;
				} else {
					$row = clean_data($row);
					$array['bill_desk_record'][] = array_combine($headers , $row);
				}
			}

		} catch (Exception $e) {

			throw new Exception("Please select proper wallet!");

		}

		$this->xlsxFileUpload($caseId, $array);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($array['bill_desk_record'] as $key => $value) {
			$array['bill_desk_record'][$key] = array_merge($value,$add_column);
		}

		BillDesk::insert($array['bill_desk_record']);

		return TRUE;
	}

	function ccAvenueAddData($file, $caseId)
	{
		$headers = array('auth_code','ccavenue_reference_no','order_card_type','order_card_name','order_bill_name','order_bill_address','order_bill_city','order_bill_state','order_bill_zip','order_bill_country','order_bill_telephone','order_bill_email','order_ip','order_url','order_no','order_date_time','order_amount','order_currency','order_status','order_bank_qsi_no','order_reciept_no');

		$rows = excel_reader($file);
		$array = array();
		$header = TRUE;

		try {

			foreach ($rows as $row) {
				if($header) {
					next($row);
					$header = FALSE;
				} else {
					$row = clean_data($row);
					$array['ccavenue_record'][] = array_combine($headers , $row);
				}
			}

		} catch (Exception $e) {

			throw new Exception("Please select proper wallet!");

		}

		$this->xlsxFileUpload($caseId, $array);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($array['ccavenue_record'] as $key => $value) {
			$array['ccavenue_record'][$key] = array_merge($value,$add_column);
		}

		CcAvenue::insert($array['ccavenue_record']);

		return TRUE;
	}

	function flipkartAddData($file, $caseId)
	{
		$headers = array('transaction_time','merchant_txn_id','amount','card_number','card_bank','first_name','email_id','phone_number','user_ip_address','shipping_address','shipping_city','shipping_state','order_id','order_status','product');

		$rows = excel_reader($file);
		$array = array();

		$header = TRUE;

		try {

			foreach ($rows as $row) {
				if($header) {
					next($row);
					$header = FALSE;
				} else {
					$row = clean_data($row);
					$array['flipkart_record'][] = array_combine($headers , $row);
				}
			}

		} catch (Exception $e) {

			throw new Exception("Please select proper wallet!");

		}

		$this->xlsxFileUpload($caseId, $array);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($array['flipkart_record'] as $key => $value) {
			$array['flipkart_record'][$key] = array_merge($value,$add_column);
		}

		Flipkart::insert($array['flipkart_record']);

		return TRUE;
	}

	function freechargeAddData($file, $caseId)
	{
		$headers = array('order_id','pg_id','amount','card_no','ip_address','email_id','transaction_date_time','registered_mob_no','status');

		$rows = excel_reader($file);
		$array = array();
		$header = TRUE;

		try {

			foreach ($rows as $row) {
				if($header) {
					next($row);
					$header = FALSE;
				} else {
					$row = clean_data($row);
					$array['freecharge_record'][] = array_combine($headers , $row);
				}
			}

		} catch (Exception $e) {

			throw new Exception("Please select proper wallet!");

		}

		$this->xlsxFileUpload($caseId, $array);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($array['freecharge_record'] as $key => $value) {
			$array['freecharge_record'][$key] = array_merge($value,$add_column);
		}

		Freecharge::insert($array['freecharge_record']);

		return TRUE;
	}

	function mPesaAddData($file, $caseId)
	{
		$headers = array('entity_circle','entity_type','entity_name','entity_wallet_no','entity_id','transaction_id','transaction_state','from_account_number','to_account_number','amount','transaction_start_time','transaction_end_time','service_name','transaction_status','transaction_failure_reason','bank_name','ifsc_code','source_channel_type','source_ip','source_port','destination_ip','destination_port','device_used','device_make_model','os_application_Version','agent_msisdn','assistant_msisdn','telescopic_flag');

		$rows = excel_reader($file,TRUE);
		$array = array();

		try {

			foreach ($rows as $row) {
				$header = TRUE;
				$check_headers = $row[0];
				foreach ($row as $data) {
					if(in_array($data[0],$check_headers)) {
						next($data);
						$header = FALSE;
					} else {
						$data = clean_data(array_slice($data,0,28));
						$array['m_pesa_record'][] = array_combine($headers , $data);
					}
				}
			}

		} catch (Exception $e) {

			throw new Exception("Please select proper wallet!");

		}

		$this->xlsxFileUpload($file, $caseId, $array);

		$add_column = [
			'case_record_id' => $caseId,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
			'uploaded_file' => $this->uploadedFile->id,
		];

		foreach ($array['m_pesa_record'] as $key => $value) {
			$array['m_pesa_record'][$key] = array_merge($value,$add_column);
		}

		MPesa::insert($array['m_pesa_record']);

		return TRUE;

	}

	public function xlsxFileUpload($caseId, $array)
	{
		$sha1 = sha1($array);

		if (UploadedFile_exists($caseId, $sha1)) {

			throw new Exception("File already exists!");

		}

		$path = 'caserecord/'.$caseId.'/wallet/';

		$data = [
			'case_record_id' => $caseId,
			'option' => 'option1',
			'for' => $_REQUEST['wallet'],
			'path' => $path,
			'name' => $this->fileValue->name_generated,
			'original_name' => $this->fileValue->name,
			'sha1' => $sha1,
			'created_by' => auth()->user()->id,
			'updated_by' => auth()->user()->id,
		];

		return $this->uploadedFile = UploadedFile::create($data);

	}

	public function destroyUploadedFile($id)
	{
		return UploadedFile::destroy($id);
	}

	public function deleteXlsxFile($fileNameWithPath)
	{
		return Storage::disk('public')->delete('xlsx/'.$fileNameWithPath);
	}

	// public function viewData(Request $request, $caseEncryptId)
	// {
	// 	$caseId = param_decrypt($caseEncryptId);

	// 	$caseRecord = CaseRecord::findOrFail($caseId);

	// 	$walletType = WalletType::all();

	// 	if (!empty($request->wallet)) {

	// 		$walletId = $request->wallet;

	// 		$selectedWalletType = WalletType::findOrFail($walletId);

	// 		$walletName = $selectedWalletType->name;
	// 		$modelName = explode(",",$selectedWalletType->model_name);

	// 		$dataArray = array();

	// 		$fields = ['case_record_id', 'uploaded_file', 'created_by', 'updated_by', 'created_at', 'updated_at'];

	// 		foreach ($modelName as $key => $value) {
	// 			$Table = constant('MODEL_PATH').$value;
	// 			$result = $Table::where('case_record_id', $caseId)->get();
	// 			$result = $result->makeHidden($fields)->toArray();
	// 			array_push($dataArray, $result);
	// 		}

	// 		if (count($dataArray[0]) > 0) {
	// 			$modelName = $selectedWalletType->model_name;
	// 			return view('panel.wallet.view-data', compact('dataArray', 'caseEncryptId', 'walletType', 'modelName'));
	// 		} else {
	// 			return redirect()->route('wallet.data.view', $caseEncryptId)->with('warning', $walletName.'  no record found!');
	// 		}

	// 	} else {

	// 		return view('panel.wallet.view-data', compact('walletType', 'caseEncryptId'));

	// 	}
	// }

	public function viewData(Request $request, $caseEncryptId)
	{
		$caseId = param_decrypt($caseEncryptId);

		$caseRecord = CaseRecord::findOrFail($caseId);

		$walletType = WalletType::all();

		if (!empty($request->wallet)) {

			$walletId = $request->wallet;

			$dataArray = array();

			$selectedWalletType = WalletType::findOrFail($walletId);

			$walletName = $selectedWalletType->name;
			$modelName = explode(",",$selectedWalletType->model_name);

			foreach ($modelName as $key => $value) {
				array_push($dataArray, columns_name($value));
			}

			if (count($dataArray) > 0) {
				$modelName = $selectedWalletType->model_name;
				return view('panel.wallet.view-data', compact('dataArray', 'caseEncryptId', 'walletType', 'modelName'));
			} else {
				return redirect()->route('wallet.data.view', $caseEncryptId)->with('warning', $walletName.'  no record found!');
			}

		} else {

			return view('panel.wallet.view-data', compact('walletType', 'caseEncryptId'));

		}
	}

}