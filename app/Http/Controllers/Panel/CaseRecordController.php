<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Exception;
use App\Models\CaseRecord;
use App\Models\CaseType;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use TCPDF;
use App\Models\PaytmTxn;
use App\Models\PaytmTxnPerson;
use App\Models\PaytmTxnHistory;
use App\Models\WalletType;
use App\Models\AirtelMoney;
use App\Models\BillDesk;
use App\Models\CcAvenue;
use App\Models\Flipkart;
use App\Models\Freecharge;
use App\Models\MPesa;

use App\Models\CdrAirtel;
use App\Models\CdrBSNL;
use App\Models\CdrIdea;
use App\Models\CdrJio;
use App\Models\CdrTelenor;
use App\Models\CdrVodafone;

use App\Models\UploadedFile;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class CaseRecordController extends Controller
{
	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('privilege')->only('create', 'store', 'assign', 'destroy', 'assignCaseRecord');
		$this->middleware('CheckCaseRecordAssign')->only('show', 'edit', 'update', 'pdfCaseRecord', 'isMalicious', 'typesTableData');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$caseRecord = CaseRecord::orderBy('id', 'desc')->with('caseType', 'assignedTo');

		// check case assign
		if (auth()->user()->privilege() === FALSE) {
			$caseRecord->where('assigned_to', auth()->user()->id);
		}

		$caseRecord = $caseRecord->get();

		return view('panel.case-record.index', compact('caseRecord'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$CaseType = CaseType::all();
		return view('panel.case-record.create', compact('CaseType'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->validate(
			[
				'name' => 'required|string',
				'mobileNumber' => 'required|numeric|digits:10',
				'email' => 'required|email',
				'address' => 'required|string',
				'caseType' => 'required|numeric|exists:case_type,id',
				"remarks" => 'nullable|string',
				'priority' => 'nullable|numeric|digits_between:1,'.count(constant('PRIORITY')),
				'expected_to_close' => 'nullable|date|after_or_equal:'.date('d-m-Y'),
				'application_from' => 'nullable|numeric|digits_between:1,'.count(constant('APPLICATION_FROM')),
				'case_tranfer_to' => 'nullable|numeric|digits_between:1,'.count(constant('CASE_TRANSFER_TO')),
				'image' => 'nullable|image',
			],
			[
				'*.digits_between' => 'Sorry please select valid item!',
			]
		);

		// Store request in valiable
		$name = $request->name;
		$mobileNumber = $request->mobileNumber;
		$email = $request->email;
		$address = $request->address;
		$caseType = $request->caseType;
		$remarks = $request->remarks;
		$priority = $request->priority;
		$expectedToClose = $request->expected_to_close;
		$applicationFrom = $request->application_from;
		$caseTranferTo = $request->case_tranfer_to;
		$image = $request->image;

		// Generate case id
		$lastCaseId = CaseRecord::select('id')->orderBy('id', 'DESC')->first();
		!empty($lastCaseId)?$last_case_id=$lastCaseId->id:$last_case_id=0;
		$case_id = date('Y').'/'.date('M').'/'.($last_case_id+1);

		// Create caseRecord
		$data = [
			'case_id' => $case_id,
			'applicant_name' => $name,
			'applicant_mobile' => $mobileNumber,
			'applicant_email' => $email,
			'applicant_address' => $address,
			'case_status' => 1,
			'case_type' => $caseType,
			'remarks' => $remarks,
			'priority' => $priority,
			'application_from' => $applicationFrom,
			'case_tranfer_to' => $caseTranferTo,
			'image' => $request->hasFile('image') ? $image->hashName() : NULL,
			'created_by' => Auth::user()->id,
			'updated_by' => Auth::user()->id,
		];

		if (auth()->user()->privilege()) {

			$extra = [
				'expected_to_close' => !empty($expectedToClose) ? date('Y-m-d', strtotime($expectedToClose)) : NULL,
			];

			$data = array_merge($data, $extra);

		}

		$caseRecord = CaseRecord::updateOrCreate($data);

		return redirect()->route('case-record.show', param_encrypt($caseRecord->id))->with('success', 'Add CaseRecord Successfully!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($caseEncryptId)
	{
		$caseId = param_decrypt($caseEncryptId);

		$caseRecord = CaseRecord::with('caseType', 'assignedTo', 'createdBy', 'updatedBy')->where('id', $caseId)->firstOrFail();

		if (auth()->user()->privilege() === FALSE) {
			return view('panel.case-record.show', compact('caseRecord'));
		} else {
			$user = User::all();
			return view('panel.case-record.show', compact('caseRecord', 'user'));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($caseEncryptId)
	{
		$caseId = param_decrypt($caseEncryptId);
		$caseRecord = CaseRecord::findOrFail($caseId);
		$CaseType = CaseType::all();
		return view('panel.case-record.edit', compact('caseRecord', 'CaseType'));		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $caseEncryptId)
	{
		$request->validate(
			[
				'name' => 'required|string',
				'mobileNumber' => 'required|numeric|digits:10',
				'email' => 'required|email',
				'address' => 'required|string',
				'caseType' => 'required|numeric|exists:case_type,id',
				"remarks" => 'nullable|string',
				'priority' => 'nullable|numeric|digits_between:1,'.count(constant('PRIORITY')),
				'expected_to_close' => 'nullable|date|after_or_equal:'.date('d-m-Y'),
				'application_from' => 'nullable|numeric|digits_between:1,'.count(constant('APPLICATION_FROM')),
				'case_tranfer_to' => 'nullable|numeric|digits_between:1,'.count(constant('CASE_TRANSFER_TO')),
			],
			[
				'*.digits_between' => 'Sorry please select valid item!',
			]
		);

		// Store request in valiable
		$name = $request->name;
		$mobileNumber = $request->mobileNumber;
		$email = $request->email;
		$address = $request->address;
		$caseType = $request->caseType;
		$remarks = $request->remarks;
		$caseId = param_decrypt($caseEncryptId);
		$priority = $request->priority;
		$expectedToClose = $request->expected_to_close;
		$applicationFrom = $request->application_from;
		$caseTranferTo = $request->case_tranfer_to;

		// Update caseRecord
		$data = [
			'applicant_name' => $name,
			'applicant_mobile' => $mobileNumber,
			'applicant_email' => $email,
			'applicant_address' => $address,
			'case_status' => 1,
			'case_type' => $caseType,
			'priority' => $priority,
			'application_from' => $applicationFrom,
			'case_tranfer_to' => $caseTranferTo,
			'remarks' => $remarks,
			'updated_by' => Auth::user()->id,
		];

		if (auth()->user()->privilege()) {

			$extra = [
				'expected_to_close' => !empty($expectedToClose) ? date('Y-m-d', strtotime($expectedToClose)) : NULL,
			];

			$data = array_merge($data, $extra);

		}

		CaseRecord::find($caseId)->update($data);

		return redirect()->route('case-record.show', $caseEncryptId)->with('success', 'Successfully update caserecord!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($caseEncryptId)
	{
		CaseRecord::destroy(param_decrypt($caseEncryptId));
		return redirect()->route('case-record.index');
	}

	/**
	 * Assign the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function assignCaseRecord(Request $request, $caseEncryptId)
	{
		$request->validate([
			'user' => 'required|numeric|exists:users,id',
		]);
		$caseId = param_decrypt($caseEncryptId);
		$assignTo = $request->user;

		$history = [
			['assignTo' => $assignTo,'timestamp' => time()]
		];

		$caseRecord = CaseRecord::findOrFail($caseId);
		$caseHistory = json_decode($caseRecord->history);
		if (is_array($caseHistory)) {
			$history = array_merge($caseHistory,$history);
		}

		$caseRecord->history = json_encode($history);
		$caseRecord->assigned_to = $assignTo;
		$caseRecord->save();

		return redirect()->route('case-record.show', compact('caseEncryptId'))->with('success', 'Successfully Assign!');
	}

	public function pdfCaseRecord(Request $request, $caseEncryptId)
	{
		// Define variables
		$temp1 = array();
		$data = array();
		$models = array();
		$fields = ['case_record_id', 'uploaded_file', 'created_by', 'updated_by', 'created_at', 'updated_at'];

		$caseId = param_decrypt($caseEncryptId);
		$caseRecord = CaseRecord::findOrFail($caseId);
		$tableName = array();

		$walletType = WalletType::all();

		foreach ($walletType as $key => $value) {
			$temp1 = explode(',', $value->model_name);
			$models = array_merge($models, $temp1);
		}

		foreach ($models as $key => $value) {
			$model = constant('MODEL_PATH').$value;

			if (class_exists($model)) {

				$tableData = $model::where(
					[
						['case_record_id', $caseId],
						['is_malicious', '1'],
					]
				)->get();

				$tableData = $tableData->makeHidden($fields)->toArray();

				if (count($tableData) > 0) {
					array_push($data, $tableData);
					array_push($tableName, $model);
				}

			} else {
				continue;
			}
		}

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// print_r($pdf);exit();

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Ahmedabad Crime Cell');
		$pdf->SetTitle('Case record ' . $caseRecord->id);
		// $pdf->SetSubject('TCPDF Tutorial');
		// $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// ---------------------------------------------------------

		// set font
		// $pdf->SetFont('times', 'BI', 15);

		// add a page
		$pdf->AddPage();

		// set some text to print
		$header = 'Case record ' . $caseRecord->id;
		$content = '';
		$tblBody = '';
		$tblHead = '';

		foreach ($data as $key => $table_data) {

			$table = '<h3>'.substr($tableName[$key], 11).'</h3><table style="width: 638px;" cellspacing="0"><thead><tr>';
			$tbl_footer = '</tbody></table>';

			$table_header = array_keys((array)$table_data[0]);
			foreach($table_header as $head => $header1) {
				if ($head < 5) {
					if($header1 != 'is_malicious') {
						$tblHead .='
							<th style="border: 1px solid #000000; text-align:center">'.ucfirst($header1).'</th>
						';
					}
				}
			}
			$tblHead .= '</tr></thead><tbody>';
			foreach($table_data as $key1 => $value) {
				$tblBody .='<tr>';
				foreach($table_header as $head => $header1) {
					if ($head < 5) {
						if($header1 != 'is_malicious') {
							$tblBody .= '
								<td style="border: 1px solid #000000; text-align:center">'.$value[$header1].'</td>
							';
						}
					}
				}
				$tblBody .='</tr>';
			}
			$content .= $table . $tblHead . $tblBody . $tbl_footer . '<h1></h1>';
			$tblHead = '';
			$tblBody = '';
		}

		// return $content;

		$pdf->SetFont('', 'B', 20);
		$pdf->Cell(0, 5, $header, 0, 1, 'C');
		$pdf->SetFont('', '', 13);
		$pdf->Ln(10);
		if (count($data) > 0) {
			$pdf->writeHTML($content, true, false, false, false);
		} else {
			$pdf->writeHTML('No data found!', true, false, false, false, 'C');
		}


		// print a block of text using Write()
		// $pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

		// ---------------------------------------------------------

		//Close and output PDF document
		ob_end_clean();
		return $pdf->Output('case-record-'.$caseRecord->id.'.pdf', 'I');

		//============================================================+
		// END OF FILE
		//============================================================+
	}

	public function files($caseEncryptId)
	{
		$caseId = param_decrypt($caseEncryptId);

		$UploadedFile = UploadedFile::where('case_record_id', $caseId)->get();

		return view('panel.case-record.files', compact('UploadedFile', 'caseEncryptId'));
	}

	public function fileDestroy(Request $request, $caseEncryptId, $uploadedFileEncryptId)
	{

		try {

			// $caseId = param_decrypt($caseEncryptId);
			$uploadedFileId = param_decrypt($uploadedFileEncryptId);

			// $UploadedFile = UploadedFile::findOrFail($uploadedFileId);
			// $Model = constant('MODEL_PATH').constant('OPTION')[$UploadedFile->option];

			// if (class_exists($Model)) {

			// 	// first delete recode from table relate to uploaded id
			// 	$Models = explode(',', $Model::findOrFail($UploadedFile->for)->model_name);
			// 	$recordDeleted = array();

			// 	foreach ($Models as $key => $value) {
			// 		$value = constant('MODEL_PATH').$value;
			// 		$recordDeleted[] = $value::where('uploaded_file',$uploadedFileId)->delete();
			// 	}

			// } else {

			// 	throw new Exception();

			// }

			// delete file from storage
			// $deleteFile = delete_file($Model::essence('directory').'/'.$UploadedFile->name);
			// $deleteFile = delete_file($UploadedFile->path.$UploadedFile->name);

			// and finally destroy record from uploaded table
			UploadedFile::destroy($uploadedFileId);


			return redirect()->route('case-record.files', $caseEncryptId)->with('success', 'Successfully delete file!');

		} catch (Exception $e) {

			return redirect()->route('case-record.files', $caseEncryptId)->with('error', $e->getMessage());

		}

	}

	public function isMalicious(Request $request, $caseEncryptId)
	{

		$error = [
			'message' => 'Something went wrong!',
		];

		try {

			$caseId = param_decrypt($caseEncryptId);
			$value = $request->value;
			$check = $request->check;

			$Model = constant('MODEL_PATH').param_decrypt($request->encrypt1);

			$tableNo = $value[0];
			$id = substr($value, 2);

			if (class_exists($Model)) {
				$result = $Model::where('id', $id)
				->where('case_record_id', $caseId)
				->update(['is_malicious' => ($check === 'true') ? '1' : '0']);
			} else {

				throw new Exception();

			}

			if ($result === 1) {
				$data = [
					'name' => 'table-danger',
					'check' => $check,
					'tableNo' => $tableNo,
				];

				return response()->json($data);
			} else {

				return response()->json($error);

			}

		} catch (Exception $e) {

			// return $e->getMessage();
			return response()->json($error);

		}

	}

	public function typesTableData(Request $request, $caseEncryptId)
	{
		// $error = [
		// 	'message' => 'Something went wrong!',
		// ];

		// try {

			$value1 = $request->value1;
			$value2 = $request->value2;
			$tableNo = $request->value3;

			$Model = constant('MODEL_PATH').constant('OPTION')[$value1];

			// if (class_exists($Model)) {

				$ModelOfModels = $Model::findOrFail($value2);
				$ModelOfModels = explode(",",$ModelOfModels->model_name);
				$ModelOfModel = constant('MODEL_PATH').$ModelOfModels[$tableNo];

				// if (class_exists($ModelOfModel)) {

					$caseId = param_decrypt($caseEncryptId);
					$result = array();

					$result = Datatables::of($ModelOfModel::where('case_record_id', $caseId))
					->setRowClass(function ($data){
						return $data->is_malicious == 1 ? 'table-danger' : '' ;
					})
					->editColumn('#', function ($data) use ($caseEncryptId,$ModelOfModels,$tableNo) {
						return "<input id=$tableNo-$data->id type='checkbox'".(($data->is_malicious == 1) ? 'checked' : '') ." onclick=isMalicious('$tableNo-$data->id',this,'$caseEncryptId','".param_encrypt($ModelOfModels[$tableNo])."')>";
					})
					->rawColumns(['#'])
					->make(true);

					return $result;

				// } else {

				// 	throw new Exception();

				// }

			// } else {

			// 	throw new Exception();

			// }

		// } catch (Exception $e) {

		// 	return response()->json($error);

		// }

	}

	public function searchImei(Request $request)
	{
		if ($request->filled('value')) {

			$value = $request->value;

			$results = array();

			// printf($value);exit();

			$results['Cdr Airtel'] = CdrAirtel::searchImei($value);
			$results['Cdr Vodafone'] = CdrVodafone::searchImei($value);
			$results['Cdr BSNL'] = CdrBSNL::searchImei($value);
			$results['Cdr Idea'] = CdrIdea::searchImei($value);
			$results['Cdr Jio'] = CdrJio::searchImei($value);
			$results['Cdr Telenor'] = CdrTelenor::searchImei($value);

			return view('panel.case-record.imei-search', compact('results'));

		} else {

			return view('panel.case-record.imei-search');

		}

	}

}
