<?php

use App\Models\Designation;
use App\Models\CaseType;
use App\Models\User;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Models\UploadedFile;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use Illuminate\Support\Facades\Schema;

function designation_name($id)
{
	$designation = Designation::findOrFail($id);
	return $designation->name;
}

function asset_storage($value = NULL)
{
	if (is_null($value)) {
		return asset('storage/');
	} else {
		return asset('storage/'.$value);
	}
}

function act_inact($value)
{
	if ($value == 1) {
		return 'Active';
	} else {
		return 'Inactive';
	}

}

function user_name($id)
{
	if ($id > 0 && $user = User::find($id)) {
		return $user->name;
	} else {
		return FALSE;
	}
}

function case_type_name($id)
{
	$caseType = CaseType::findOrFail($id);
	return $caseType->name;
}

function excel_reader($inputFileName, $multiple=FALSE)
{
	/**  Identify the type of $inputFileName  **/
	$inputFileType = IOFactory::identify($inputFileName->path());
	/**  Create a new Reader of the type that has been identified  **/
	$reader = IOFactory::createReader($inputFileType);
	/**  Load $inputFileName to a Spreadsheet Object  **/
	$spreadsheet = $reader->load($inputFileName->path());
	if($multiple)
	{  
		$rows_data = array();
		$i = 1;
		 foreach ($spreadsheet->getAllSheets() as $sheet) {
			$rows_data[$sheet->getTitle()] = $sheet->toArray();
		}
		return $rows_data;
	} else {
		$worksheet = $spreadsheet->getActiveSheet();
		$rows = $worksheet->toArray();
		return $rows;
	}
}

function csv_reader($file, $header, $tableFields)
{
	echo "<pre>";

	$hkey = NULL;
	$fkey = NULL;
	$columns = 0;
	$csvMap = array();
	$csvData = array();

	$columns = count($header);

	$reader = new Csv;
	$spreadsheet = $reader->load($file->path());
	$csvMap = $spreadsheet->getActiveSheet()->toArray();

	foreach ($csvMap as $key => $value) {
		if ($header === array_filter($value)) {

			$hkey = $key;
			break;

		}
	}

	// print_r($csvMap);exit();

	if ($hkey === NULL) {

		$csvMap = array_map('str_getcsv', file($file));

		foreach ($csvMap as $key => $value) {
			if ($header === array_filter($value)) {

				$hkey = $key;
				break;

			}
		}

	}

	foreach (array_reverse($csvMap) as $key => $value) {
		if ( count(array_filter($value)) > 3 ) {
			$fkey = ++$key;
			break;
		}
	}

	$fkey = count($csvMap)-$fkey;

	if ( is_int($hkey) && !is_null($hkey) && $columns > 0 && !empty($csvMap) && $fkey >= 0 ) {

		array_walk($csvMap, function($row, $key) use ($csvMap, &$csvData, $header, &$tableFields, &$hkey, &$fkey, &$columns) {

			$trimArray = [];

			if ($header !== $row && $key > $hkey && $key <= $fkey) {

				if ( count(array_filter($row)) > 3 ) {

					foreach ($row as $rowkey => $rowValue) {

						if ($rowkey < $columns) {

							$trimArray[] = trim($rowValue, "'");

						}

					}

					$csvData[] = array_combine($tableFields, $trimArray);

				}

				$trimArray = [];

			}

		});

	} else{

		throw new Exception("File not valid!");

	}

	if (!empty($csvData)) {

		return $csvData;

	} else {

		throw new Exception("Empty csv record!");

	}

}

function clean_data($data)
{
	$clean_data = array();
	
	foreach ($data as $value) {
		$type = gettype($value);

		if($type == 'double')
		{
			$fval = number_format($value, 0, '', '');
			$clean_data[] = $fval;
		}
		else if($type == 'string')
		{
			$fval = trim($value);
			$clean_data[] = $fval;
		}
		else if($type == 'NULL')
		{
			$fval = "";
			$clean_data[] = $fval;
		}
		else
		{
			$fval = $value;
			$clean_data[] = $fval;
		}
	}
	return $clean_data;
}


function param_encrypt($value)
{
	// Set your unique has keys	
	 $secretKey = 'LKFZ'; 
	  $secretIv = 'WEBBD161';
	// Encryption method
	  $encryptMethod = "AES-256-CBC"; 

	// pass string/number which you want to encrypt
	$key = hash('sha256', $secretKey);
	$iv = substr(hash('sha256', $secretIv), 0, 16);
	$result = openssl_encrypt($value, $encryptMethod, $key, 0, $iv);
	return $result= base64_encode($result);
} 

function param_decrypt($value=null)
{
	// Set your unique has keys	
	$secretKey = 'LKFZ'; 
	$secretIv = 'WEBBD161';
	// Encryption method
	$encryptMethod = "AES-256-CBC"; 

	if($value==null) {
		return null;
	}
	$key = hash('sha256', $secretKey);
	$iv = substr(hash('sha256', $secretIv), 0, 16);
	$result = openssl_decrypt(base64_decode($value),$encryptMethod, $key, 0, $iv);
	return $result;
}

function upload_fileAs($path, $file, $fileName)
{
	return basename(Storage::disk('public')->putFileAs($path, new File($file), $fileName));
}

function upload_file($path, $file)
{
	return basename(Storage::disk('public')->put($path, $file));
}

function delete_file($fileNameWithPath)
{

	// print_r($fileNameWithPath);exit();
	return Storage::disk('public')->delete($fileNameWithPath);
}

function UploadedFile_exists($caseId, $sha1)
{
	return UploadedFile::exists($caseId, $sha1)->first();
}

function file_nest($file, $request_file=NULL)
{

	$variable = pathinfo($file['name']);

	// remove extention
	// substr($file->hashName(), 0, strrpos($file->hashName(), '.'));

	$name_only = $variable['filename'];
	$extension = $variable['extension'];

	$name_generated = $name_only.'_'.uniqid().'.'.$extension;

	$value = [
		'name_only' => $name_only,
		'extension' => $extension,
		'name_generated' => $name_generated,
	];

	return (Object)$file = array_merge($file, $value);
}

function import_data($query, $data)
{
	foreach (array_chunk($data, 500) as $key => $value) {
		$query->insert($value);
	}

	return $query;
}

function columns_name($name)
{
	$Model = constant('MODEL_PATH').$name;

	$columns = Schema::getColumnListing(with(new $Model)->getTable());
	
	$fields = ['id','case_record_id', 'is_malicious', 'uploaded_file', 'created_by', 'updated_by', 'created_at', 'updated_at'];

	return array_values(array_diff($columns, $fields));

}

function define_option($option=NULL, $key=NULL)
{
	try {

		$Model = constant('MODEL_PATH').constant('OPTION')[$option];

		if (class_exists($Model)) {

			try {

				return $Model::essence($key);

			} catch (Exception $e) {

				return NULL;

			}

		} else {

			return NULL;

		}

	} catch (Exception $e) {

		return NULL;

	}

}