<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\CaseRecord;

class CheckCaseRecordAssign
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($request->route()->parameters());
        $caseId = param_decrypt($request->caseEncryptId);

        if (auth()->user()->privilege() === TRUE) {

            return $next($request);

        } else {

            CaseRecord::where('id', $caseId)
            ->where('assigned_to', auth()->user()->id)
            ->firstOrFail();

            return $next($request);

        }
    }
}
