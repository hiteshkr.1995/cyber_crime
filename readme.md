# Cyber Crime

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
git clone https://gitlab.com/hiteshkr.1995/cyber_crime.git
```

### Next goto directry

```
cd cyber_crime
```

### Next composer command

```
composer install
```

### Next copy env and set you creadential

```
cp .env.example .env
```

### Next generate app key

```
php artisan key:generate
```

### On Permission issues use

```
sudo chmod -R 777 storage
```
```
sudo chmod -R 777 bootstrap
```

### Create tables

```
php artisan migrate
```

### Create symbolic link

```
php artisan storage:link
```

### On success of migration for insert Designation

```
php artisan db:seed
```

## Built With

* [Laravel](https://laravel.com/) - Web Framework
* [Bootstrap](http://getbootstrap.com/) - Frontend Framework

## Versioning

We use for versioning. For the versions available, see this repository

## Authors

* **Naresh Kumar** - *Initial work and project Assistance*
* **Hitesh Kumar** - *Work as Developer*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
